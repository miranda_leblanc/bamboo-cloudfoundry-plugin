/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.admin;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import net.java.ao.EntityManager;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;
import net.java.ao.test.junit.ActiveObjectsJUnitRunner;

import org.gaptap.bamboo.cloudfoundry.admin.DefaultCloudFoundryAdminServiceTest.TestData;
import org.gaptap.bamboo.cloudfoundry.admin.tasks.CloudFoundryTaskConfigurationService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.test.TestActiveObjects;
import com.atlassian.bamboo.build.DefaultJob;
import com.atlassian.bamboo.build.Job;
import com.google.common.collect.Lists;

/**
 * @author David Ehringer
 * 
 */
@RunWith(ActiveObjectsJUnitRunner.class)
@Data(TestData.class)
@Ignore
public class DefaultCloudFoundryAdminServiceTest {

	private EntityManager entityManager;
	private ActiveObjects ao;
	private CloudFoundryTaskConfigurationService taskService;
	private CloudFoundryAdminService adminService;

	@Before
	public void setUp() throws Exception {
		assertNotNull(entityManager);
		ao = new TestActiveObjects(entityManager);
		taskService = mock(CloudFoundryTaskConfigurationService.class);
		adminService = new DefaultCloudFoundryAdminService(ao, taskService);
	}

	@Test
	public void targetsCanBeAdded() {
		Target target = adminService.addTarget("My Target", "http://api.localhost", "A test target.", true, 1, 1, true);
		ao.flushAll();
		assertThat(target.getID(), is(not(0)));
	}

	@Test
	public void targetsCanBeUpdated() {
		int id = 1;
		String url = "http://myurl";
		String name = "New name";
		String description = null;
		Credentials credentials = adminService.getCredentials(2);
		Proxy proxy = null;

		adminService.updateTarget(id, name, url, description, true, credentials.getID(), null, false);
		ao.flushAll();

		Target target = adminService.getTarget(id);
		assertThat(target.getCredentials(), is(credentials));
		assertThat(target.getDescription(), is(description));
		assertThat(target.getName(), is(name));
		assertThat(target.getProxy(), is(proxy));
		assertThat(target.getUrl(), is(url));
		assertThat(target.isTrustSelfSignedCerts(), is(true));
        assertThat(target.isDisableForBuildPlans(), is(false));
	}

	@Test
	public void targetsCanBeDeletedIfTheyAreNotUsedByAnyTasks() {
		Target target = adminService.getTarget(1);
		assertThat(target, is(not(nullValue())));

		adminService.deleteTarget(1);
		ao.flushAll();

		target = adminService.getTarget(1);
		assertThat(target, is(nullValue()));
	}

	@Test
	public void ifATargetIsUsedByJobsAndYouTryToDeleteItATargetInUseExceptionIsThrownThatContainsAllTheJobsUsingIt() {
		Target target = adminService.getTarget(1);
		assertThat(target, is(not(nullValue())));

		List<Job> jobsUsingTarget = Lists.newArrayList();
		Job job1 = new DefaultJob();
		Job job2 = new DefaultJob();
		jobsUsingTarget.add(job1);
		jobsUsingTarget.add(job2);
		when(taskService.allJobsUsingTarget(target)).thenReturn(jobsUsingTarget);

		boolean exceptionThrown = false;
		try {
			adminService.deleteTarget(1);
			ao.flushAll();
		} catch (InUseByJobException e) {
			exceptionThrown = true;
			List<Job> jobs = e.getJobsUsing();
			assertThat(jobs.size(), is(2));
			assertThat(jobs, hasItems(job1, job2));
		}
		assertThat(exceptionThrown, is(true));
	}

	@Test
	public void proxiesCanBeAdded() {
		Proxy proxy = adminService.addProxy("test-proxy", "proxy.cloudfoundry.com", 80, null);
		ao.flushAll();
		assertThat(proxy.getID(), is(not(0)));
	}

	@Test
	public void proxiesCanBeUpdated() {
		int id = 1;
		String description = "Test update";
		String host = "updated-host.cloudfoundry.com";
		String name = "updated-name";
		int port = 8080;

		Proxy proxy = adminService.getProxy(id);
		assertThat(proxy.getDescription(), is(not(description)));
		assertThat(proxy.getHost(), is(not(host)));
		assertThat(proxy.getName(), is(not(name)));
		assertThat(proxy.getPort(), is(not(port)));

		adminService.updateProxy(id, name, host, port, description);
		assertThat(proxy.getDescription(), is(description));
		assertThat(proxy.getHost(), is(host));
		assertThat(proxy.getName(), is(name));
		assertThat(proxy.getPort(), is(port));
	}

	@Test
	public void proxiesCanBeDeletedIfTheyAreNotUsedByAnyTargets() {
		Proxy proxy = adminService.getProxy(1);
		assertThat(proxy, is(not(nullValue())));

		adminService.deleteProxy(1);
		ao.flushAll();
		proxy = adminService.getProxy(1);
		assertThat(proxy, is(nullValue()));
	}

	@Test
	public void ifAProxyIsUsedByTargetsAndYouTryToDeleteItATargetInUseExceptionIsThrownThatContainsAllTheTargetsUsingIt() {
		Proxy proxy = adminService.getProxy(2);
		assertThat(proxy, is(not(nullValue())));

		Target target1 = adminService.getTarget(1);
		assertThat(target1.getProxy(), is(proxy));

		Target target2 = adminService.getTarget(2);
		assertThat(target2.getProxy(), is(proxy));

		boolean exceptionThrown = false;
		try {
			adminService.deleteProxy(2);
			ao.flushAll();
		} catch (InUseByTargetException e) {
			exceptionThrown = true;
			List<Target> targets = e.getTargetsUsing();
			assertThat(targets, hasItems(target1, target2));
		}
		assertThat(exceptionThrown, is(true));
	}

	@Test
	public void credentialsCanBeAdded() {
		Credentials credentials = adminService.addCredentials("my-creds", "cf-user", "cf-password", null);
		ao.flushAll();
		assertThat(credentials.getID(), is(not(0)));
	}

	@Test
	public void credentialsCanBeDeletedIfTheyAreNotUsedByAnyTargets() {
		Credentials credentials = adminService.getCredentials(1);
		assertThat(credentials, is(not(nullValue())));

		adminService.deleteCredentials(1);
		ao.flushAll();
		credentials = adminService.getCredentials(1);
		assertThat(credentials, is(nullValue()));
	}

	@Test
	public void ifACredentialsIsUsedByTargetsAndYouTryToDeleteItATargetInUseExceptionIsThrownThatContainsAllTheTargetsUsingIt() {
		Credentials credentials = adminService.getCredentials(2);
		assertThat(credentials, is(not(nullValue())));

		Target target1 = adminService.getTarget(1);
		assertThat(target1.getCredentials(), is(credentials));

		Target target2 = adminService.getTarget(2);
		assertThat(target2.getCredentials(), is(credentials));

		boolean exceptionThrown = false;
		try {
			adminService.deleteCredentials(2);
			ao.flushAll();
		} catch (InUseByTargetException e) {
			exceptionThrown = true;
			List<Target> targets = e.getTargetsUsing();
			assertThat(targets, hasItems(target1, target2));
		}
		assertThat(exceptionThrown, is(true));
	}

	public static class TestData implements DatabaseUpdater {

		@SuppressWarnings("unchecked")
		@Override
		public void update(EntityManager em) throws Exception {
			em.migrate(Proxy.class);
			em.migrate(Credentials.class);
			em.migrate(Target.class);

			Proxy proxy = em.create(Proxy.class);
			proxy.setName("My Proxy");
			proxy.setHost("proxy.davidehringer.com");
			proxy.setPort(80);
			proxy.save();

			proxy = em.create(Proxy.class);
			proxy.setName("My Alternative Proxy");
			proxy.setHost("proxy-2.davidehringer.com");
			proxy.setPort(443);
			proxy.save();

			Credentials credentials = em.create(Credentials.class);
			credentials.setName("My Credentials");
			credentials.setUsername("dehringer");
			credentials.setPassword("myPassword");
			credentials.save();

			credentials = em.create(Credentials.class);
			credentials.setName("Admin Credentials");
			credentials.setUsername("admin");
			credentials.setPassword("adminPassword");
			credentials.save();

			Target target = em.create(Target.class);
			target.setName("Cloud Foundry.com");
			target.setUrl("http://api.cloudfoundry.com");
			target.setCredentials(credentials);
			target.setProxy(proxy);
			target.save();

			target = em.create(Target.class);
			target.setName("AppFog");
			target.setUrl("http://api.appfog.com");
			target.setCredentials(credentials);
			target.setProxy(proxy);
			target.save();
		}
	}
}
