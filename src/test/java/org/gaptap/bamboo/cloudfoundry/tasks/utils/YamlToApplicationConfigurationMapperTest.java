/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;

import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.Log4jLogger;
import org.gaptap.bamboo.cloudfoundry.client.ServiceManifest;
import org.junit.Before;
import org.junit.Test;

import com.google.common.collect.Maps;

/**
 * @author David Ehringer
 * 
 */
public class YamlToApplicationConfigurationMapperTest {

    private InputStream min;
    private InputStream full;
    private InputStream fullSymbols;
    private InputStream fullCustomVariables;
    private InputStream bambooVariables;

    private InputStream diskG;
    private InputStream diskGb;
    private InputStream diskMb;
    private InputStream diskLowerM;
    private InputStream diskMissingSuffix;

    private InputStream memoryGb;

    private InputStream leanServices;
    private InputStream url;
    private InputStream urls;
    private InputStream invalidTimeout;
    private InputStream hostNoDomain;
    private InputStream hosts;

    private InputStream deprecatedDisk;
    private InputStream deprecatedMem;

    private InputStream routes;
    private InputStream routesWithSymbols;
    private InputStream noRoute;

    private InputStream domainsWithHost;
    private InputStream domainsWithHosts;

    private InputStream noHostname;

    private InputStream noHealthCheckType;
    private InputStream healthCheckType;

    private YamlToApplicationConfigurationMapper mapper;

    @Before
    public void setup() throws FileNotFoundException {

        min = new FileInputStream(new File("src/test/resources/manifest-min.yml"));
        full = new FileInputStream(new File("src/test/resources/manifest-full.yml"));
        fullSymbols = new FileInputStream(new File("src/test/resources/manifest-full-symbols.yml"));
        fullCustomVariables = new FileInputStream(new File("src/test/resources/manifest-full-custom-variables.yml"));
        bambooVariables = new FileInputStream(new File("src/test/resources/manifest-bamboo-variables.yml"));

        memoryGb = new FileInputStream(new File("src/test/resources/manifest-memory-GB.yml"));

        diskG = new FileInputStream(new File("src/test/resources/manifest-disk-G.yml"));
        diskGb = new FileInputStream(new File("src/test/resources/manifest-disk-GB.yml"));
        diskMb = new FileInputStream(new File("src/test/resources/manifest-disk-MB.yml"));
        diskLowerM = new FileInputStream(new File("src/test/resources/manifest-disk-m.yml"));
        diskMissingSuffix = new FileInputStream(new File("src/test/resources/manifest-disk-missing-suffix.yml"));

        leanServices = new FileInputStream(new File("src/test/resources/manifest-lean-services.yml"));
        url = new FileInputStream(new File("src/test/resources/manifest-url-symbols.yml"));
        urls = new FileInputStream(new File("src/test/resources/manifest-urls-symbols.yml"));
        invalidTimeout = new FileInputStream(new File("src/test/resources/manifest-invalid-timeout.yml"));
        hostNoDomain = new FileInputStream(new File("src/test/resources/manifest-host-no-domain.yml"));
        hosts = new FileInputStream(new File("src/test/resources/manifest-hosts.yml"));

        deprecatedDisk = new FileInputStream(new File("src/test/resources/manifest-deprecated-disk.yml"));
        deprecatedMem = new FileInputStream(new File("src/test/resources/manifest-deprecated-mem.yml"));

        routes = new FileInputStream(new File("src/test/resources/manifest-routes.yml"));
        routesWithSymbols = new FileInputStream(new File("src/test/resources/manifest-routes-symbols.yml"));
        noRoute = new FileInputStream(new File("src/test/resources/manifest-no-route.yml"));

        domainsWithHost = new FileInputStream(new File("src/test/resources/manifest-domains-host.yml"));
        domainsWithHosts = new FileInputStream(new File("src/test/resources/manifest-domains-hosts.yml"));

        noHostname = new FileInputStream(new File("src/test/resources/manifest-no-hostname.yml"));

        noHealthCheckType = new FileInputStream(new File("src/test/resources/manifest-no-healthcheck.yml"));
        healthCheckType = new FileInputStream(new File("src/test/resources/manifest-healthcheck.yml"));

        mapper = new YamlToApplicationConfigurationMapper("run.pivotal.io", new Log4jLogger());
    }

    @Test
    public void leanServices() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(leanServices, getBambooVariables());
        assertThat(config.name(), is("app"));
        assertThat(config.instances(), is(1));
        assertThat(config.memory(), is(1024));
        assertThat(config.serviceBindings().size(), is(3));
    }

    @Test
    public void minimumNumberOfEntriesProvided() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(min, getBambooVariables());
        assertThat(config.name(), is("hello-manifest"));
        assertThat(config.instances(), is(1));
        assertThat(config.memory(), is(1024));
        assertThat(config.diskQuota(), is(ApplicationConfiguration.DEFAULT_DISK_QUOTA));
        assertThat(config.command(), is(nullValue()));
        assertThat(config.buildpackUrl(), is(nullValue()));
        assertThat(config.environment().size(), is(0));
        assertThat(config.routes(), is(empty()));
        assertThat(config.healthCheckTimeout(), is(ApplicationConfiguration.PLATFORM_DEFAULT_TIMEOUT));
    }

    @Test
    public void allEntriesProvided() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(full, getBambooVariables());
        assertThat(config.name(), is("hello-manifest"));
        assertThat(config.instances(), is(1));
        assertThat(config.memory(), is(128));
        assertThat(config.diskQuota(), is(2048));
        assertThat(config.command(), is("bundle exec rake server:start_command"));
        assertThat(config.buildpackUrl(), is("https://github.com/cloudfoundry/heroku-buildpack-ruby.git"));
        assertThat(config.healthCheckTimeout(), is(80));
        assertThat(config.stack(), is("cflinuxfs2"));

        assertThat(config.environment().size(), is(2));
        assertThat(config.environment().get("greeting"), is("hello"));
        assertThat(config.environment().get("BUNDLE_WITHOUT"), is("test:development"));

        ServiceManifest service1 = ServiceManifest.builder()
                .label("rediscloud")
                .name("rediscloud-8cccc")
                .plan("20mb")
                .build();
        ServiceManifest service2 = ServiceManifest.builder()
                .label("mongolab")
                .name("mongolab-6d208")
                .plan("sandbox")
                .build();

        assertThat(config.hosts().size(), is(1));
        assertThat(config.hosts(), contains("my-app"));
        assertThat(config.domains().size(), is(1));
        assertThat(config.domains(), contains("ctapps.io"));
    }
    
    ///////////// Custom Variables

    @Test
    public void customSymbolsAreResolvedInTheName() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.name(), is("hello-manifest"));
    }

    @Test
    public void customSymbolsAreResolvedInTimeout() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.healthCheckTimeout(), is(100));
    }

    @Test
    public void customSymbolsAreResolvedInEnvironmentVariables() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.environment().get("JAVA_OPTS"), startsWith("-Drandom.value=run.pivotal.io."));
        assertThat(config.environment().get("JAVA_OPTS"),
                endsWith(" -Dspring.profiles.active=test -Dlog.dir=/app/logs/hello-manifest -Denvironment=test"));
        assertThat(config.environment().get("JAVA_OPTS"), not(containsString("${random-word}")));
        assertThat(config.environment().get("app-name"), is("hello-manifest"));
        assertThat(config.environment(), hasKey("e-hello-manifest"));
    }

    @Test
    public void customSymbolsAreResolvedInTheHostAndDomainVariables() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.hosts().get(0), startsWith("my-hello-manifest-"));
        assertThat(config.hosts().get(0), not(containsString("${random-word}")));
        assertThat(config.domains().get(0), endsWith("run.pivotal.io"));
    }

    @Test
    public void customSymbolsAreResolvedInTheBuildpackVariable() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.buildpackUrl(), is("https://github.com/cloudfoundry/hello-manifest-buildpack.git"));
    }

    @Test
    public void customSymbolsAreResolvedInTheCommandVariable() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.command(), startsWith("bin/demo/hello-manifest/run.pivotal.io/run "));
        assertThat(config.command(), not(endsWith(("/run ${random-word}"))));
    }

    @Test
    public void customSymbolsTheInstanceFieldCanBeUsedInVariableResolution() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.environment().get("count"), is("1"));
    }

    @Test
    public void customSymbolsTheMemFieldCanBeUsedInVariableResolution() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.environment().get("size"), is("128M"));
    }

    @Test
    public void customSymbolsStandardStringFieldsCanBeUsedInVariableResolution() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullCustomVariables, getBambooVariables());

        assertThat(config.environment().get("where"), startsWith("my-hello-manifest"));
        assertThat(config.environment().get("where"), not(endsWith(("${random-word}"))));

        assertThat(config.environment().get("owner"), is("run.pivotal.io"));

        assertThat(config.environment().get("alt-command"),
                startsWith("bin/demo/hello-manifest/run.pivotal.io/run "));
        assertThat(config.environment().get("alt-command"), not(endsWith(("/run ${random-word}"))));

        assertThat(config.environment().get("builder"),
                is("https://github.com/cloudfoundry/hello-manifest-buildpack.git"));
    }
    
    ////////////
    
    ///////////// Internal Symbols

    @Test
    public void symbolsAreResolvedInEnvironmentVariables() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.environment().get("JAVA_OPTS"), startsWith("-Drandom.value=run.pivotal.io."));
        assertThat(config.environment().get("JAVA_OPTS"),
                endsWith(" -Dspring.profiles.active=test -Dlog.dir=/app/logs/hello-manifest"));
        assertThat(config.environment().get("JAVA_OPTS"), not(containsString("${random-word}")));
        assertThat(config.environment().get("app-name"), is("hello-manifest"));
        assertThat(config.environment(), hasKey("e-hello-manifest"));
    }

    @Test
    public void symbolsAreResolvedInTheHostAndDomainVariables() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.hosts().get(0), startsWith("my-hello-manifest-"));
        assertThat(config.hosts().get(0), not(containsString("${random-word}")));
        assertThat(config.domains().get(0), endsWith("run.pivotal.io"));
    }

    @Test
    public void symbolsAreResolvedInTheBuildpackVariable() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.buildpackUrl(), is("https://github.com/cloudfoundry/hello-manifest-buildpack.git"));
    }

    @Test
    public void symbolsAreResolvedInTheCommandVariable() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.command(), startsWith("bin/demo/hello-manifest/run.pivotal.io/run "));
        assertThat(config.command(), not(endsWith(("/run ${random-word}"))));
    }

    @Test
    public void theInstanceFieldCanBeUsedInVariableResolution() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.environment().get("count"), is("1"));
    }

    @Test
    public void theMemFieldCanBeUsedInVariableResolution() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.environment().get("size"), is("128M"));
    }

    @Test
    public void standardStringFieldsCanBeUsedInVariableResolution() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(fullSymbols, getBambooVariables());

        assertThat(config.environment().get("where"), startsWith("my-hello-manifest"));
        assertThat(config.environment().get("where"), not(endsWith(("${random-word}"))));

        assertThat(config.environment().get("owner"), is("run.pivotal.io"));

        assertThat(config.environment().get("alt-command"),
                startsWith("bin/demo/hello-manifest/run.pivotal.io/run "));
        assertThat(config.environment().get("alt-command"), not(endsWith(("/run ${random-word}"))));

        assertThat(config.environment().get("builder"),
                is("https://github.com/cloudfoundry/hello-manifest-buildpack.git"));
    }
    
    ////////

    ///////////// Bamboo Variables
    
    private Map<String, String> getBambooVariables(){
        Map<String, String> variables = Maps.newHashMap();
        variables.put("my-bamboo-name", "hello-manifest");
        variables.put("my.bamboo-instances", "1");
        variables.put("my.bamboo.memory", "128M");
        variables.put("my-bamboo-timeout", "100");
        variables.put("my-bamboo-disk", "1024M");
        return variables;
    }

    @Test
    public void bambooVariablesAreResolvedInTheName() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.name(), is("hello-manifest"));
    }

    @Test
    public void bambooVariablesAreResolvedInTimeout() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.healthCheckTimeout(), is(100));
    }

    @Test
    public void bambooVariablesAreResolvedInInstances() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.instances(), is(1));
    }

    @Test
    public void bambooVariablesAreResolvedInDiskQuota() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.diskQuota(), is(1024));
    }
    
    @Test
    public void bambooVariablesAreResolvedInTheHostAndDomainVariables() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.hosts().get(0), startsWith("my-hello-manifest-"));
        assertThat(config.hosts().get(0), not(containsString("${random-word}")));
        assertThat(config.domains().get(0), endsWith("run.pivotal.io"));
    }

    @Test
    public void bambooVariablesAreResolvedInTheBuildpackVariable() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.buildpackUrl(), is("https://github.com/cloudfoundry/hello-manifest-buildpack.git"));
    }

    @Test
    public void bambooVariablesAreResolvedInTheCommandVariable() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(bambooVariables, getBambooVariables());

        assertThat(config.command(), startsWith("bin/demo/hello-manifest/run.pivotal.io/run "));
        assertThat(config.command(), not(endsWith(("/run ${random-word}"))));
    }
    
    ////////////

    @Test
    public void whenDiskQuotaIsSpecifiedInGItIsConvertedToMb() {
        ApplicationConfiguration config = mapper.from(diskG, getBambooVariables());
        assertThat(config.diskQuota(), is(1024));
    }

    @Test
    public void whenDiskQuotaIsSpecifiedInGbItIsConvertedToMb() {
        ApplicationConfiguration config = mapper.from(diskGb, getBambooVariables());
        assertThat(config.diskQuota(), is(2048));
    }

    @Test
    public void diskInM() {
        ApplicationConfiguration config = mapper.from(full, getBambooVariables());
        assertThat(config.diskQuota(), is(2048));
    }

    @Test
    public void diskInMb() {
        ApplicationConfiguration config = mapper.from(diskMb, getBambooVariables());
        assertThat(config.diskQuota(), is(2048));
    }

    @Test
    public void diskInLowerM() {
        ApplicationConfiguration config = mapper.from(diskLowerM, getBambooVariables());
        assertThat(config.diskQuota(), is(2048));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenDiskQuotaDoesNotHaveASuffixAnExceptionIsThrown() {
        mapper.from(diskMissingSuffix, getBambooVariables());
    }

    @Test
    public void deprecatedDiskAttribute() throws FileNotFoundException {
        // TODO remove support in future version
        ApplicationConfiguration config = mapper.from(deprecatedDisk, getBambooVariables());
        assertThat(config.diskQuota(), is(2048));
    }


    @Test
    public void memoryInGb() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(memoryGb, getBambooVariables());
        assertThat(config.memory(), is(2048));
    }

    @Test
    public void deprecatedMemAttribute() throws FileNotFoundException {
        // TODO remove support in future version
        ApplicationConfiguration config = mapper.from(deprecatedMem, getBambooVariables());
        assertThat(config.memory(), is(128));
    }

    @Test
    public void aListOfUrlsCanBeProvided() {
        // - url:
        ApplicationConfiguration config = mapper.from(url, getBambooVariables());
        assertUrlsExist(config);

        // - urls:
        config = mapper.from(urls, getBambooVariables());
        assertUrlsExist(config);
    }

    private void assertUrlsExist(ApplicationConfiguration config) {
        assertThat(config.routes().size(), is(3));
        assertThat(config.routes().get(0), is("my-urls-manifest-run.pivotal.io"));
        assertThat(config.routes().get(1), is("test.com"));
        assertThat(config.routes().get(2), is("url.com"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenTimeouIsNotAnIntegerAnIllegalArgumentExceptionIsThrown() {
        mapper.from(invalidTimeout, getBambooVariables());
    }
    
    @Test
    public void whenAHostIsProvidedWithoutADomainThePlatformDomainIsUsed() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(hostNoDomain, getBambooVariables());

        assertThat(config.hosts().size(), is(1));
        assertThat(config.hosts().get(0), is("my-app"));
        assertThat(config.domains().size(), is(1));
        assertThat(config.domains().get(0), is("run.pivotal.io"));
    }
    
    @Test
    public void bothHostAndHostsCanBeProvided() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(hosts, getBambooVariables());

        assertThat(config.routes().size(), is(0));

        assertThat(config.hosts().size(), is(3));
        assertThat(config.hosts().get(0), is("my-app"));
        assertThat(config.hosts().get(1), is("my-app2"));
        assertThat(config.hosts().get(2), is("my-app3"));
    }

    @Test
    public void routes() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(routes, getBambooVariables());

        assertThat(config.routes().size(), is(3));
        assertTrue(config.routes().contains("example.com"));
        assertTrue(config.routes().contains("www.example.com/foo"));
        assertTrue(config.routes().contains("tcp-example.com:1234"));
    }

    @Test
    public void routesWithSymbols() throws FileNotFoundException {
        ApplicationConfiguration config = mapper.from(routesWithSymbols, getBambooVariables());

        assertThat(config.routes().size(), is(3));
        assertTrue(config.routes().contains("test-value.example.com/foo"));
    }

    @Test
    public void domainsWithHost(){
        ApplicationConfiguration config = mapper.from(domainsWithHost, getBambooVariables());

        assertThat(config.routes().size(), is(0));

        assertThat(config.domains().size(), is(2));
        assertTrue(config.domains().contains("example.com"));
        assertTrue(config.domains().contains("acme.com"));

        assertThat(config.hosts().size(), is(1));
        assertTrue(config.hosts().contains("my-app"));

    }

    @Test
    public void domainsWithHosts(){
        ApplicationConfiguration config = mapper.from(domainsWithHosts, getBambooVariables());

        assertThat(config.routes().size(), is(0));

        assertThat(config.domains().size(), is(2));
        assertTrue(config.domains().contains("example.com"));
        assertTrue(config.domains().contains("acme.com"));
        assertThat(config.hosts().size(), is(2));
        assertTrue(config.hosts().contains("my-app"));
        assertTrue(config.hosts().contains("my-app"));
    }

    @Test
    public void healthCheckTypeNotSpecifiedDefaultsToEmpty(){
        ApplicationConfiguration config = mapper.from(noHealthCheckType, getBambooVariables());

        assertThat(config.healthCheckType(), is(nullValue()));
    }

    @Test
    public void healthCheckTypeSpecified(){
        ApplicationConfiguration config = mapper.from(healthCheckType, getBambooVariables());

        assertThat(config.healthCheckType(), is("none"));
    }

    @Test
    public void noRouteTrue(){
        ApplicationConfiguration config = mapper.from(noRoute, getBambooVariables());

        assertTrue(config.noRoute());
    }

    @Test
    public void noRouteEmpty(){
        ApplicationConfiguration config = mapper.from(min, getBambooVariables());

        assertNull(config.noRoute());
    }

    @Test
    public void noHostname(){
        ApplicationConfiguration config = mapper.from(noHostname, getBambooVariables());

        assertTrue(config.noHostname());
    }

}
