/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.cli;


import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.build.logger.BuildLogger;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationSummary;
import org.cloudfoundry.operations.applications.InstanceDetail;
import org.cloudfoundry.operations.services.ServiceInstance;
import org.cloudfoundry.operations.services.ServiceInstanceSummary;
import org.cloudfoundry.operations.services.ServiceInstanceType;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CliFormatterTest {

    private static final long ONE_GB = 1073741824L;

    private BuildLogger logger;
    private CliFormatter formatter;

    @Before
    public void setup(){
        logger = new StubbedBuildLogger();
        formatter = new CliFormatter(logger);
    }

    @Test
    public void cfApp() throws ParseException {

        DateFormat lastUploadedDateFormat = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss zzz");
        Date lastUploadedDate = lastUploadedDateFormat.parse("11/24/2015 03:15:46 UTC");

        DateFormat sinceDateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");

        ApplicationDetail detail = ApplicationDetail.builder()
                .id("123")
                .requestedState("started")
                .instances(3)
                .runningInstances(3)
                .url("example.cf.com")
                .lastUploaded(lastUploadedDate)
                .stack("cflinuxfs2")
                .buildpack("Static file")
                .diskQuota(1073741824)
                .memoryLimit(1024) // in ApplicationDetail, memory is in MB rather than bytes
                .name("example")
                .instanceDetail(
                        InstanceDetail.builder()
                                .index("0")
                                .since(sinceDateFormat.parse("2016-08-17 04:26:10 PM"))
                                .state("running")
                                .cpu(6.43545345)
                                .diskQuota(ONE_GB)
                                .diskUsage(9122611L)
                                .memoryQuota(ONE_GB)
                                .memoryUsage(7130316L)
                                .build(),
                        InstanceDetail.builder()
                                .index("1")
                                .since(sinceDateFormat.parse("2016-08-30 03:01:14 PM"))
                                .state("running")
                                .cpu(24.04)
                                .diskQuota(ONE_GB)
                                .diskUsage(9122611L)
                                .memoryQuota(ONE_GB)
                                .memoryUsage(3565158L)
                                .build())
                .build();

        formatter.cfApp(detail);

        List<LogEntry> logEntries = logger.getLastNLogEntries(25);
        assertThat(logEntries.get(1).getLog(), is("requested state: started"));
        assertThat(logEntries.get(2).getLog(), is("instances: 3/3"));
        assertThat(logEntries.get(3).getLog(), is("usage: 1G x 3 instances"));
        assertThat(logEntries.get(4).getLog(), is("urls: example.cf.com"));
        assertThat(logEntries.get(5).getLog(), is("last uploaded: Tue Nov 24 03:15:46 UTC 2015"));
        assertThat(logEntries.get(6).getLog(), is("stack: cflinuxfs2"));
        assertThat(logEntries.get(7).getLog(), is("buildpack: Static file"));

        assertThat(logEntries.get(9).getUnstyledLog(), is("     state     since                    cpu     memory       disk         details   "));
        assertThat(logEntries.get(10).getUnstyledLog(), is("#0   running   2016-08-17 04:26:10 PM   6.4%    6.8M of 1G   8.7M of 1G   "));
        assertThat(logEntries.get(11).getUnstyledLog(), is("#1   running   2016-08-30 03:01:14 PM   24.0%   3.4M of 1G   8.7M of 1G   "));
    }

    @Test
    public void cfAppWithNoRunningInstances() throws ParseException {

        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy KK:mm:ss zzz");
        Date date = dateFormat.parse("11/24/2015 03:15:46 UTC");

        ApplicationDetail detail = ApplicationDetail.builder()
                .id("123")
                .requestedState("started")
                .instances(3)
                .runningInstances(3)
                .url("example.cf.com")
                .lastUploaded(date)
                .stack("cflinuxfs2")
                .buildpack("Static file")
                .diskQuota(1073741824)
                .memoryLimit(1073741824)
                .name("example")
                .build();

        formatter.cfApp(detail);

        List<LogEntry> logEntries = logger.getLastNLogEntries(10);

        assertThat(logEntries.get(9).getLog(), is("There are no running instances of this app."));
    }

    @Test
    public void cfApps(){
        List<ApplicationSummary> summaries = new ArrayList<>();
        summaries.add(ApplicationSummary.builder()
                .id("")
                .name("bamboo")
                .diskQuota(1073741824/(1024 * 1024))
                .memoryLimit(1073741824/(1024 * 1024))
                .requestedState("started")
                .instances(2)
                .runningInstances(1)
                .build());

        summaries.add(ApplicationSummary.builder()
                .id("")
                .name("my-app")
                .diskQuota(1073741824/(1024 * 1024))
                .memoryLimit(1073741824/(1024 * 1024))
                .requestedState("started")
                .instances(2)
                .runningInstances(2)
                .url("my-app.com", "another.com")
                .build());

        formatter.cfApps(summaries);

        List<LogEntry> logEntries = logger.getLastNLogEntries(10);
        assertThat(logEntries.get(1).getUnstyledLog(), is("name     requested state   instances   memory   disk   urls                      "));
        assertThat(logEntries.get(2).getUnstyledLog(), is("bamboo   started           1/2         1G       1G                               "));
        assertThat(logEntries.get(3).getUnstyledLog(), is("my-app   started           2/2         1G       1G     my-app.com, another.com   "));
    }

    // TODO cf apps with no apps

    @Test
    public void cfServices(){
        // TODO
        List<ServiceInstanceSummary> services = new ArrayList<>();
        services.add(ServiceInstanceSummary.builder()
                .id("")
                .type(ServiceInstanceType.USER_PROVIDED)
                .name("my-key")
                .service("user-provided")
                .build());

        services.add(ServiceInstanceSummary.builder()
                .id("")
                .type(ServiceInstanceType.MANAGED)
                .name("example-config-server")
                .service("p-config-server")
                .plan("standard")
                .application("my-app")
                .lastOperation("create")
                .build());

        services.add(ServiceInstanceSummary.builder()
                .id("")
                .type(ServiceInstanceType.MANAGED)
                .name("auto-scaler")
                .service("app-autoscaler")
                .plan("bronze")
                .application("metrics", "my-app")
                .lastOperation("create")
                .build());

        formatter.cfServices(services);

        List<LogEntry> logEntries = logger.getLastNLogEntries(10);
        assertThat(logEntries.get(1).getUnstyledLog(), is("name                    service           plan       bound apps        last operation   "));
        assertThat(logEntries.get(2).getUnstyledLog(), is("my-key                  user-provided                                                   "));
        assertThat(logEntries.get(3).getUnstyledLog(), is("example-config-server   p-config-server   standard   my-app            create           "));
        assertThat(logEntries.get(4).getUnstyledLog(), is("auto-scaler             app-autoscaler    bronze     metrics, my-app   create           "));
    }

    @Test
    public void cfServiceUserProvided(){
        ServiceInstance serviceInstance = ServiceInstance.builder()
                .id("")
                .type(ServiceInstanceType.USER_PROVIDED)
                .name("my-key")
                .service("user-provided")
                .build();

        formatter.cfService(serviceInstance);

        List<LogEntry> logEntries = logger.getLastNLogEntries(10);
        assertThat(logEntries.get(1).getUnstyledLog(), is("Service instance: my-key"));
        assertThat(logEntries.get(2).getUnstyledLog(), is("Service: user-provided"));
        assertThat(logEntries.size(), is(3));
    }

    @Test
    public void cfServiceManaged(){
        ServiceInstance serviceInstance = ServiceInstance.builder()
                .id("")
                .type(ServiceInstanceType.MANAGED)
                .name("auto-scaler")
                .service("app-autoscaler")
                .plan("bronze")
                .application("metrics", "my-app")
                .description("RabbitMQ is a robust and scalable high-performance multi-protocol messaging broker.")
                .documentationUrl("http://docs.pivotal.io")
                .dashboardUrl("https://pivotal-rabbitmq.cf.com/#/login/")
                .lastOperation("create")
                .status("succeeded")
                .startedAt("2016-03-09T05:15:43Z")
                .updatedAt("2015-11-05T17:22:00Z")
                .message("Some message")
                .build();

        formatter.cfService(serviceInstance);

        List<LogEntry> logEntries = logger.getLastNLogEntries(15);
        assertThat(logEntries.get(1).getUnstyledLog(), is("Service instance: auto-scaler"));
        assertThat(logEntries.get(2).getUnstyledLog(), is("Service: app-autoscaler"));
        assertThat(logEntries.get(3).getUnstyledLog(), is("Plan: bronze"));
        assertThat(logEntries.get(4).getUnstyledLog(), is("Description: RabbitMQ is a robust and scalable high-performance multi-protocol messaging broker."));
        assertThat(logEntries.get(5).getUnstyledLog(), is("Documentation url: http://docs.pivotal.io"));
        assertThat(logEntries.get(6).getUnstyledLog(), is("Dashboard: https://pivotal-rabbitmq.cf.com/#/login/"));
        assertThat(logEntries.get(7).getUnstyledLog(), is(""));
        assertThat(logEntries.get(8).getUnstyledLog(), is("Last Operation"));
        assertThat(logEntries.get(9).getUnstyledLog(), is("Status: create succeeded"));
        assertThat(logEntries.get(10).getUnstyledLog(), is("Message: Some message"));
        assertThat(logEntries.get(11).getUnstyledLog(), is("Started: 2016-03-09T05:15:43Z"));
        assertThat(logEntries.get(12).getUnstyledLog(), is("Updated: 2015-11-05T17:22:00Z"));
    }
}
