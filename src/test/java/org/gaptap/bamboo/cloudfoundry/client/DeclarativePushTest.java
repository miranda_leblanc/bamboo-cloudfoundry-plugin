/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsRequest;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsResponse;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationEnvironments;
import org.cloudfoundry.operations.applications.GetApplicationEnvironmentsRequest;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import org.cloudfoundry.operations.applications.PushApplicationRequest;
import org.cloudfoundry.operations.routes.ListRoutesRequest;
import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.cloudfoundry.operations.routes.Route;
import org.cloudfoundry.operations.routes.UnmapRouteRequest;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DeclarativePushTest extends AbstractJavaClientTest {

    private AccumulatingLogger logger;
    private CloudFoundryService cloudFoundryService;
    private DeclarativePush push;
    private PushConfiguration testPushConfiguration;

    @Before
    public void setup(){
        logger = new AccumulatingLogger();

        cloudFoundryService = mock(CloudFoundryService.class);
        push = new DeclarativePush(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, cloudFoundryService);

        testPushConfiguration = PushConfiguration.builder()
                .applicationFile(new File("src/test/resources"))
                .start(false)
                .startupTimeout(180)
                .build();
    }

    private void requestPush(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .applications()
                .push(any(PushApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestGetApp(CloudFoundryService cloudFoundryService){
        when(cloudFoundryService
                .getApp(anyString()))
                .thenReturn(Mono
                        .just(buildGenericApplicationDetail()));
    }

    private void requestGetApp(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations
                .applications()
                .get(GetApplicationRequest.builder()
                        .name(anyString())
                        .build()))
                .thenReturn(Mono.just(buildGenericApplicationDetail()));
    }

    private void requestApp(CloudFoundryService cloudFoundryService){
        when(cloudFoundryService
                .app(anyString()))
                .thenReturn(Mono
                        .just(buildGenericApplicationDetail()));
    }

    @NotNull
    private ApplicationDetail buildGenericApplicationDetail() {
        return ApplicationDetail.builder()
                .id("123")
                .name("unit-test")
                .stack("cflinuxfs2")
                .diskQuota(1024 * 1024)
                .instances(1)
                .memoryLimit(1024 * 1024)
                .requestedState("started")
                .runningInstances(1)
                .build();
    }

    private void requestListServiceBindings(CloudFoundryClient cloudFoundryClient) {
        when(cloudFoundryClient
                .applicationsV2()
                .listServiceBindings(any(ListApplicationServiceBindingsRequest.class)))
                .thenReturn(Mono.just(ListApplicationServiceBindingsResponse.builder()
                        .totalPages(1)
                        .totalResults(0)
                        .addAllResources(new ArrayList<>())
                        .build()));
    }

    private void requestEnvironment(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .applications()
                .getEnvironments(any(GetApplicationEnvironmentsRequest.class)))
                .thenReturn(Mono.just(ApplicationEnvironments.builder()
                        .build()));
    }

    private void requestListRoutes(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .routes()
                .list(any(ListRoutesRequest.class)))
                .thenReturn(Flux.<Route>empty());
    }

    private void requestMapRoute(CloudFoundryService cloudFoundryService) {
        when(cloudFoundryService.map((any(MapRouteRequest.class))))
                .thenReturn(Mono.empty());
    }

    private void requestUnmapRoute(CloudFoundryService cloudFoundryService) {
        when(cloudFoundryService.unmap((any(UnmapRouteRequest.class))))
                .thenReturn(Mono.empty());
    }

    private void requestStartApp(CloudFoundryService cloudFoundryService) {
        when(cloudFoundryService.startApp(anyString(), anyInt(), anyInt()))
                .thenReturn(Mono.empty());
    }

    private void standardTestMocks() {
        requestPush(cloudFoundryOperations);
        requestGetApp(cloudFoundryService);
        requestGetApp(cloudFoundryOperations);
        requestApp(cloudFoundryService);
        requestListServiceBindings(cloudFoundryClient);
        requestEnvironment(cloudFoundryOperations);
        requestListRoutes(cloudFoundryOperations);
        requestMapRoute(cloudFoundryService);
        requestUnmapRoute(cloudFoundryService);
        requestStartApp(cloudFoundryService);
    }

    private void noRouteTestMocks() {
        requestPush(cloudFoundryOperations);
        requestGetApp(cloudFoundryService);
        requestApp(cloudFoundryService);
        requestListServiceBindings(cloudFoundryClient);
        requestEnvironment(cloudFoundryOperations);
        requestListRoutes(cloudFoundryOperations);
        requestMapRoute(cloudFoundryService);
        requestUnmapRoute(cloudFoundryService);
    }

    @Test
    @Ignore("This feature resulted in gettting 'InvalidRouteRelation' errors in some environments. Need to explore this use case more.")
    public void noRouteIsDisabledIfNoRoutesAreProvidedAndTheNoRoutesAttributeIsNull() throws InterruptedException {
        // This will give your app a default Route, similar to the way the cli works

        // Given
        noRouteTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(false)));
    }

    @Test
    public void noRouteIsEnabledIfRoutesAreProvidedAndTheNoRoutesAttributeIsNull() throws InterruptedException {
        // Given
        noRouteTestMocks();

        List<String> routes = new ArrayList<>();
        routes.add("test.example.com");

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .addAllRoutes(routes)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(true)));
    }

    @Test
    public void noRouteIsEnabledIfNoRoutesAreProvidedAndTheNoRoutesAttributeIsTrue() throws InterruptedException {
        // Given
        noRouteTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noRoute(true)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(true)));
    }

    @Test
    public void noRouteIsDisabledIfNoRoutesAreProvidedAndTheNoRoutesAttributeIsFalse() throws InterruptedException {
        // Given
        noRouteTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noRoute(false)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations, times(4)).applications();
        verify(cloudFoundryOperations.applications()).push(argThat(noRoute(false)));
    }

    private static ArgumentMatcher<PushApplicationRequest> noRoute(boolean enabled){
        return new ArgumentMatcher<PushApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                PushApplicationRequest request = (PushApplicationRequest) o;
                return request.getNoRoute() == enabled;
            }
        };
    }

    @Test
    public void ifNoHostnameIsTrue() throws InterruptedException {
        // Given
        standardTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noHostname(true)
                .domain("test.com")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).push(argThat(noHostname(true)));
    }

    @Test
    public void ifNoHostnameIsDefaulted() throws InterruptedException {
        // Given
        standardTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .host("test-host")
                .domain("test.com")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).push(argThat(noHostname(false)));
    }

    private static ArgumentMatcher<PushApplicationRequest> noHostname(boolean enabled){
        return new ArgumentMatcher<PushApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                PushApplicationRequest request = (PushApplicationRequest) o;
                return request.getNoHostname() == enabled;
            }
        };
    }

    @Test
    public void allRoutesAreMappedToTheApplication() throws InterruptedException {
        // Given
        standardTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .route("one.cf.com", "two.cf.com", "three.cf-2.com")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryService, times(3)).map(any(MapRouteRequest.class));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "cf.com", "one")));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "cf.com", "two")));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "cf-2.com", "three")));
    }

    private static ArgumentMatcher<MapRouteRequest> mapRouteRequest(String appName, String domain, String host){
        return new ArgumentMatcher<MapRouteRequest>() {
            @Override
            public boolean matches(Object o) {
                MapRouteRequest request = (MapRouteRequest) o;
                return request.getApplicationName().equals(appName)
                        && request.getDomain().equals(domain)
                        && request.getHost().equals(host);
            }
        };
    }

    @Test
    public void allHostDomainPairsAreMappedToTheApplication() throws InterruptedException {
        // Given
        standardTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .host("host-one", "host-two")
                .domain("domain-one.com", "domain-two.com")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryService, times(4)).map(any(MapRouteRequest.class));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "domain-one.com", "host-one")));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "domain-one.com", "host-two")));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "domain-two.com", "host-one")));
        verify(cloudFoundryService).map(argThat(mapRouteRequest("unit-test", "domain-two.com", "host-two")));
    }
    @Test
    public void unmapStaleRoutes() throws InterruptedException {
        // Given
        standardTestMocks();
        requestListRoutesWithMultiple(cloudFoundryOperations);

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .route("docs.cloudfoundry.org/latest", "host-one.cf.com")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryService, times(3)).unmap(any(UnmapRouteRequest.class));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "cloudfoundry.org", null, null)));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "dark-cloudfoundry.org", null, null)));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "cloudfoundry.org", "docs", "/previous")));
    }

    @Test
    public void unmapStaleRoutesWhenConfigurationHasNoHostname() throws InterruptedException {
        // Given
        standardTestMocks();
        requestListRoutesWithMultiple(cloudFoundryOperations);

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noHostname(true)
                .domain("cloudfoundry.org")
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, testPushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryService, times(4)).unmap(any(UnmapRouteRequest.class));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "cf.com", "host-one", null)));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "dark-cloudfoundry.org", null, null)));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "cloudfoundry.org", "docs", "/latest")));
        verify(cloudFoundryService).unmap(argThat(unmapRouteRequest("unit-test", "cloudfoundry.org", "docs", "/previous")));
    }

    private void requestListRoutesWithMultiple(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations
                .routes()
                .list(any(ListRoutesRequest.class)))
                .thenReturn(Flux
                        .fromIterable(Arrays.asList(
                                Route.builder()
                                        .id(UUID.randomUUID().toString())
                                        .space("my-space")
                                        .application("unit-test", "another-app")
                                        .domain("cf.com")
                                        .host("host-one")
                                        .path("")
                                        .build(),
                                Route.builder()
                                        .id(UUID.randomUUID().toString())
                                        .space("my-space")
                                        .application("another-app")
                                        .domain("something.com")
                                        .host("another-host")
                                        .path("")
                                        .build(),
                                Route.builder()
                                        .id(UUID.randomUUID().toString())
                                        .space("my-space")
                                        .application("unit-test")
                                        .domain("cloudfoundry.org")
                                        .host("")
                                        .path("")
                                        .build(),
                                Route.builder()
                                        .id(UUID.randomUUID().toString())
                                        .space("my-space")
                                        .application("unit-test")
                                        .domain("dark-cloudfoundry.org")
                                        .host("")
                                        .path("")
                                        .build(),
                                Route.builder()
                                        .id(UUID.randomUUID().toString())
                                        .space("my-space")
                                        .application("unit-test")
                                        .domain("cloudfoundry.org")
                                        .host("docs")
                                        .path("/latest")
                                        .build(),
                                Route.builder()
                                        .id(UUID.randomUUID().toString())
                                        .space("my-space")
                                        .application("unit-test")
                                        .domain("cloudfoundry.org")
                                        .host("docs")
                                        .path("/previous")
                                        .build())));
    }

    private static ArgumentMatcher<UnmapRouteRequest> unmapRouteRequest(String appName, String domain, String host, String path){
        return new ArgumentMatcher<UnmapRouteRequest>() {
            @Override
            public boolean matches(Object o) {
                UnmapRouteRequest request = (UnmapRouteRequest) o;
                return request.getApplicationName().equals(appName)
                        && request.getDomain().equals(domain)
                        && ((request.getHost() == null && host == null) || (request.getHost() != null && request.getHost().equals(host)))
                        && ((request.getPath() == null && path == null) || (request.getPath() != null && request.getPath().equals(path)));
            }
        };
    }

    @Test
    public void startupAndStagingTimeoutAreProvidedToTheStartAppCall(){
        // Given
        standardTestMocks();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("unit-test")
                .noRoute(true)
                .build();
        PushConfiguration pushConfiguration = PushConfiguration.builder()
                .applicationFile(new File("src/test/resources"))
                .start(true)
                .startupTimeout(500)
                .stagingTimeout(1500)
                .build();

        // When
        StepVerifier
                .create(push.push(applicationConfiguration, pushConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryService).startApp("unit-test", 500, 1500);
    }
}
