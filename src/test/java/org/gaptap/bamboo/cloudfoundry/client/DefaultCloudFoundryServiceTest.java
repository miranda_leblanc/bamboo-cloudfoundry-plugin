/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import com.google.common.collect.Lists;
import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.ClientV2Exception;
import org.cloudfoundry.client.v2.Metadata;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsRequest;
import org.cloudfoundry.client.v2.domains.DeleteDomainRequest;
import org.cloudfoundry.client.v2.organizations.ListOrganizationPrivateDomainsRequest;
import org.cloudfoundry.client.v2.organizations.ListOrganizationPrivateDomainsResponse;
import org.cloudfoundry.client.v2.privatedomains.PrivateDomainEntity;
import org.cloudfoundry.client.v2.privatedomains.PrivateDomainResource;
import org.cloudfoundry.client.v2.routes.ListRoutesResponse;
import org.cloudfoundry.client.v2.routes.RouteEntity;
import org.cloudfoundry.client.v2.routes.RouteResource;
import org.cloudfoundry.client.v2.serviceinstances.BindServiceInstanceRouteRequest;
import org.cloudfoundry.client.v2.serviceinstances.BindServiceInstanceRouteResponse;
import org.cloudfoundry.client.v2.serviceinstances.ServiceInstanceEntity;
import org.cloudfoundry.client.v2.shareddomains.ListSharedDomainsRequest;
import org.cloudfoundry.client.v2.shareddomains.ListSharedDomainsResponse;
import org.cloudfoundry.client.v2.shareddomains.SharedDomainEntity;
import org.cloudfoundry.client.v2.shareddomains.SharedDomainResource;
import org.cloudfoundry.client.v3.tasks.CancelTaskRequest;
import org.cloudfoundry.client.v3.tasks.CancelTaskResponse;
import org.cloudfoundry.client.v3.tasks.CreateTaskRequest;
import org.cloudfoundry.client.v3.tasks.CreateTaskResponse;
import org.cloudfoundry.client.v3.tasks.GetTaskRequest;
import org.cloudfoundry.client.v3.tasks.GetTaskResponse;
import org.cloudfoundry.client.v3.tasks.TaskState;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.doppler.StreamRequest;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.DeleteApplicationRequest;
import org.cloudfoundry.operations.applications.GetApplicationEnvironmentsRequest;
import org.cloudfoundry.operations.applications.GetApplicationRequest;
import org.cloudfoundry.operations.applications.PushApplicationRequest;
import org.cloudfoundry.operations.applications.RenameApplicationRequest;
import org.cloudfoundry.operations.applications.StartApplicationRequest;
import org.cloudfoundry.operations.domains.Domain;
import org.cloudfoundry.operations.domains.Status;
import org.cloudfoundry.operations.organizations.OrganizationDetail;
import org.cloudfoundry.operations.organizations.OrganizationInfoRequest;
import org.cloudfoundry.operations.organizations.OrganizationQuota;
import org.cloudfoundry.operations.routes.CreateRouteRequest;
import org.cloudfoundry.operations.routes.DeleteRouteRequest;
import org.cloudfoundry.operations.routes.ListRoutesRequest;
import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.cloudfoundry.operations.routes.UnmapRouteRequest;
import org.cloudfoundry.operations.services.CreateServiceInstanceRequest;
import org.cloudfoundry.operations.services.GetServiceInstanceRequest;
import org.cloudfoundry.operations.services.ServiceInstance;
import org.cloudfoundry.operations.services.ServiceInstanceType;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.File;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Predicate;

import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultCloudFoundryServiceTest extends AbstractJavaClientTest{

    private CloudFoundryService service;
    private Logger logger = new Log4jLogger();
    private PushConfiguration testPushConfiguration;

    private static void requestServiceInstanceNotFound(CloudFoundryOperations cloudFoundryOperations, String serviceInstanceName){
        when(cloudFoundryOperations
                .services()
                .getInstance(GetServiceInstanceRequest.builder()
                        .name(serviceInstanceName)
                        .build()))
                .thenReturn(Mono
                        .error(new IllegalArgumentException()));
    }

    private static void requestCreateServiceInstance(CloudFoundryOperations cloudFoundryOperations, ServiceConfiguration serviceConfiguration){
        when(cloudFoundryOperations
                .services()
                .createInstance(buildCreateServiceInstanceRequest(serviceConfiguration)))
                .thenReturn(Mono.empty());
    }

    private static CreateServiceInstanceRequest buildCreateServiceInstanceRequest(ServiceConfiguration serviceConfiguration) {
        return CreateServiceInstanceRequest.builder()
                .parameters(serviceConfiguration.getParameters())
                .tags(serviceConfiguration.getTags())
                .serviceInstanceName(serviceConfiguration.getServiceInstance())
                .serviceName(serviceConfiguration.getService())
                .planName(serviceConfiguration.getPlan())
                .build();
    }

    private static void requestListDomains(CloudFoundryOperations cloudFoundryOperations){
        List<Domain> domains = new ArrayList<>();
        domains.add(Domain.builder()
                .name("pivotal.io")
                .id("1")
                .status(Status.SHARED)
                .build());
        domains.add(Domain.builder()
                .name("libertymutual.com")
                .id("2")
                .status(Status.SHARED)
                .build());
        domains.add(Domain.builder()
                .name("davidehringer.com")
                .id("3")
                .status(Status.OWNED)
                .build());
        when(cloudFoundryOperations
                .domains().list())
                .thenReturn(Flux.fromIterable(domains));
    }

    private void requestDeleteDomain(CloudFoundryClient cloudFoundryClient, String domainId) {
        when(cloudFoundryClient.domains().delete(DeleteDomainRequest.builder()
                .domainId(domainId)
                .build()))
        .thenReturn(Mono.empty());
    }

    private void requestCreateRoute(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations.routes().create(any(CreateRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestDeleteRoute(CloudFoundryOperations cloudFoundryOperations) {
        when(cloudFoundryOperations.routes().delete(any(DeleteRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void pushApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().push(any(PushApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestGetApplication(CloudFoundryOperations cloudFoundryOperations){
        requestGetApplication(cloudFoundryOperations, "test-app", UUID.randomUUID().toString());
    }

    private void requestGetApplication(CloudFoundryOperations cloudFoundryOperations, String name, String id){
        ApplicationDetail applicationDetail = ApplicationDetail.builder()
                .name(name)
                .id(id)
                .stack("cflinuxfs2")
                .diskQuota(1073741824)
                .instances(2)
                .memoryLimit(1024)
                .requestedState("started")
                .runningInstances(2)
                .build();
        when(cloudFoundryOperations.applications().get(any(GetApplicationRequest.class)))
                .thenReturn(Mono.just(applicationDetail));
    }

    private void requestGetUnknownApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().get(any(GetApplicationRequest.class)))
                .thenReturn(Mono.error(new IllegalArgumentException()));
    }

    private void requestListServiceBindings(CloudFoundryClient cloudFoundryClient){
        when(cloudFoundryClient.applicationsV2().listServiceBindings(any(ListApplicationServiceBindingsRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestGetEnvironments(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().getEnvironments(any(GetApplicationEnvironmentsRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestListRoutes(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.routes().list(any(ListRoutesRequest.class)))
                .thenReturn(Flux.empty());
    }

    private void requestStartApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().start(any(StartApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestMapRoutes(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.routes().map(any(MapRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestUnmapRoutes(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.routes().unmap(any(UnmapRouteRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestStreamApplicationLogs(DopplerClient dopplerClient){
        when(dopplerClient.stream(any(StreamRequest.class)))
                .thenReturn(Flux.empty());
    }

    private void requestDeleteApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().delete(any(DeleteApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void requestRenameApplication(CloudFoundryOperations cloudFoundryOperations){
        when(cloudFoundryOperations.applications().rename(any(RenameApplicationRequest.class)))
                .thenReturn(Mono.empty());
    }

    private void setupMocksForPush() {
        pushApplication(cloudFoundryOperations);
        requestGetApplication(cloudFoundryOperations);
        requestListServiceBindings(cloudFoundryClient);
        requestGetEnvironments(cloudFoundryOperations);
        requestListRoutes(cloudFoundryOperations);
        requestStartApplication(cloudFoundryOperations);
        requestMapRoutes(cloudFoundryOperations);
        requestUnmapRoutes(cloudFoundryOperations);
        requestStreamApplicationLogs(dopplerClient);
        requestDeleteApplication(cloudFoundryOperations);
        requestRenameApplication(cloudFoundryOperations);
    }

    @Before
    public final void setup() {
        Logger logger = new Log4jLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        testPushConfiguration = PushConfiguration.builder()
                .applicationFile(new File("src/test/resources"))
                .start(false)
                .startupTimeout(60)
                .build();
    }

    @Test
    public void pushNewService() throws InterruptedException {
        // Given
        ServiceConfiguration serviceConfiguration = ServiceConfiguration.builder()
                .plan("gold")
                .service("redis")
                .serviceInstance("my-redis")
                .build();

        requestServiceInstanceNotFound(cloudFoundryOperations, serviceConfiguration.getServiceInstance());
        requestCreateServiceInstance(cloudFoundryOperations, serviceConfiguration);

        // When
        StepVerifier
                .create(service.pushService(serviceConfiguration))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.services()).createInstance(buildCreateServiceInstanceRequest(serviceConfiguration));
    }

    @Test
    public void deleteNonExistentApplication() throws InterruptedException {
        // Given
        requestGetUnknownApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.deleteApp("foo"))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications(), never()).delete(any(DeleteApplicationRequest.class));
    }

    @Test
    public void deleteApplication() throws InterruptedException {
        // Given
        requestGetApplication(cloudFoundryOperations);
        requestDeleteApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.deleteApp("foo"))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).delete(any(DeleteApplicationRequest.class));
    }

    @Test
    public void deleteDomain() throws InterruptedException {
        // Given
        requestListDomains(cloudFoundryOperations);
        requestDeleteDomain(cloudFoundryClient, "3");

        // when
        StepVerifier
                .create(service.deleteDomain("davidehringer.com"))
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryClient.domains()).delete(DeleteDomainRequest.builder()
                .domainId("3")
                .build());
    }

    @Test
    public void deleteNonExistentDomain() throws InterruptedException {
        // Given
        requestListDomains(cloudFoundryOperations);

        // when, then
        StepVerifier
                .create(service.deleteDomain("de.com"))
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void renameNonExistentApplicationFailIfNotExistTrue() throws InterruptedException {
        // Given
        requestGetUnknownApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.renameApp("non-existent", "new-name", true))
                .expectError();
    }

    @Test
    public void renameNonExistentApplicationFailIfNotExistFalse() throws InterruptedException {
        // Given
        requestGetUnknownApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.renameApp("non-existent", "new-name", false))
                .expectComplete()
                .verify();
    }

    @Test
    public void renameExistingApplication() throws InterruptedException {
        // Given
        requestGetApplication(cloudFoundryOperations);
        requestRenameApplication(cloudFoundryOperations);

        // when
        StepVerifier
                .create(service.renameApp("existent", "new-name", false))
                .expectComplete()
                .verify();

        // then
        verify(cloudFoundryOperations.applications()).rename(RenameApplicationRequest.builder()
                .name("existent")
                .newName("new-name")
                .build());
    }

    @Test
    public void createRouteDoesNotLogPathWhenItIsNotProvided() throws InterruptedException {
        // Given
        AccumulatingLogger logger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        requestCreateRoute(cloudFoundryOperations);

        // When
        StepVerifier
                .create(service.createRoute("cloudfoundry.org", "bamboo", "", "space"))
                .expectComplete()
                .verify();

        // Then
        assertTrue(logger.containsInfoLogEntry("Adding route bamboo.cloudfoundry.org..."));
        assertTrue(logger.containsInfoLogEntry("Adding route bamboo.cloudfoundry.org... OK"));
    }

    @Test
    public void createRouteLogsThePathWhenItIsProvided() throws InterruptedException {
        // Given
        AccumulatingLogger logger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        requestCreateRoute(cloudFoundryOperations);

        // When
        StepVerifier
                .create(service.createRoute("cloudfoundry.org", "bamboo", "/a-path", "space"))
                .expectComplete()
                .verify();

        // Then
        assertTrue(logger.containsInfoLogEntry("Adding route bamboo.cloudfoundry.org/a-path..."));
        assertTrue(logger.containsInfoLogEntry("Adding route bamboo.cloudfoundry.org/a-path... OK"));
    }

    @Test
    public void deleteRouteLogsThePathWhenItIsProvided() throws InterruptedException {
        // Given
        AccumulatingLogger logger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, logger, healthChecker);

        requestDeleteRoute(cloudFoundryOperations);

        // When
        StepVerifier
                .create(service.deleteRoute("cloudfoundry.org", "bamboo", "/a-path"))
                .expectComplete()
                .verify();

        // Then
        assertTrue(logger.containsInfoLogEntry("Deleting route bamboo.cloudfoundry.org/a-path..."));
        assertTrue(logger.containsInfoLogEntry("Deleting route bamboo.cloudfoundry.org/a-path... OK"));
    }

    @Test
    public void whenAHealthEndpointIsNotSpecifiedForABlueGreenDeploymentAHealthCheckIsNotExecuted() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        when(healthChecker.assertHealthy(eq(applicationConfiguration), eq(blueGreenConfiguration), any(logger.getClass())))
                .thenReturn(Mono.empty());

        // When
        StepVerifier
                .create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(healthChecker, never()).assertHealthy(eq(applicationConfiguration), eq(blueGreenConfiguration), any(logger.getClass()));
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedTheBlueGreenAppNameAndRoutesAreUsedForTheDarkApp() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkAppName("darkapp")
                .customDarkRoute("dark.cf.com")
                .build();

        // When
        StepVerifier
                .create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).push(argThat(hasAppName(blueGreenConfiguration.customDarkAppName())));
        verify(cloudFoundryOperations.routes()).map(argThat(hasMapRoute(blueGreenConfiguration.customDarkAppName(), blueGreenConfiguration.customDarkRoute())));
    }

    @Test
    public void whenABlueGreenDeploymentUrlMappingFlow() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .useCustomDarkAppConfiguration(true)
                .customDarkAppName("darkapp")
                .customDarkRoute("dark.cf.com")
                .build();

        // When
        StepVerifier
                .create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        ApplicationConfiguration darkApplicationConfiguration = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        for(String route: applicationConfiguration.routes()) {
            verify(cloudFoundryOperations.routes()).map(argThat(hasMapRoute(blueGreenConfiguration.customDarkAppName(), route)));
        }
        for(String route: darkApplicationConfiguration.routes()){
            verify(cloudFoundryOperations.routes()).unmap(argThat(hasUnmapRoute(blueGreenConfiguration.customDarkAppName(), route)));
        }
    }

    private static ArgumentMatcher<PushApplicationRequest> hasAppName(String name){
        return new ArgumentMatcher<PushApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                PushApplicationRequest request = (PushApplicationRequest) o;
                return name.equals(request.getName());
            }
        };
    }

    private static ArgumentMatcher<MapRouteRequest> hasMapRoute(String appName, String route){
        return new ArgumentMatcher<MapRouteRequest>() {
            @Override
            public boolean matches(Object o) {
                MapRouteRequest request = (MapRouteRequest) o;
                return request.equals(RouteRequestBuilder.buildMapRouteRequest(appName, route));
            }
        };
    }

    private static ArgumentMatcher<UnmapRouteRequest> hasUnmapRoute(String appName, String route){
        return new ArgumentMatcher<UnmapRouteRequest>() {
            @Override
            public boolean matches(Object o) {
                UnmapRouteRequest request = (UnmapRouteRequest) o;
                return request.equals(RouteRequestBuilder.buildUnmapRouteRequest(appName, route));
            }
        };
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedItTriesToDeleteTheDarkApp() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        // When
        StepVerifier.create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).delete(argThat(deleteRequestWithAppName("test-app-dark")));
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedTheOldAppIsDeleted() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        // When
        StepVerifier.create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).delete(argThat(deleteRequestWithAppName("test-app")));
    }

    private static ArgumentMatcher<DeleteApplicationRequest> deleteRequestWithAppName(String name){
        return new ArgumentMatcher<DeleteApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                DeleteApplicationRequest request = (DeleteApplicationRequest) o;
                return name.equals(request.getName());
            }
        };
    }

    @Test
    public void whenABlueGreenDeploymentIsExecutedTheNewAppIsRenamedToTakeTheOldAppsName() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("live.test-app.com", "live-2.test-app.com")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .build();

        // When
        StepVerifier.create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryOperations.applications()).rename(argThat(renameRequestWithAppNames("test-app-dark", "test-app")));
    }

    private static ArgumentMatcher<RenameApplicationRequest> renameRequestWithAppNames(String oldName, String newName){
        return new ArgumentMatcher<RenameApplicationRequest>() {
            @Override
            public boolean matches(Object o) {
                RenameApplicationRequest request = (RenameApplicationRequest) o;
                return oldName.equals(request.getName()) &&
                        newName.equals(request.getNewName());
            }
        };
    }

    @Test
    public void whenAHealthEndpointIsSpecifiedForABlueGreenDeploymentAHealthCheckIsExecuted() throws InterruptedException {
        // Given
        setupMocksForPush();

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();

        when(healthChecker.assertHealthy(any(ApplicationConfiguration.class), any(BlueGreenConfiguration.class), any(logger.getClass())))
                .thenReturn(Mono.empty());

        // When
        StepVerifier.create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        ApplicationConfiguration convertedConfig = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        verify(healthChecker).assertHealthy(eq(convertedConfig), eq(blueGreenConfiguration), any(logger.getClass()));
    }

    @Test
    public void whenAHealthCheckFailsTheBlueGreenDeploymentIsAborted() throws InterruptedException {
        // Given
        pushApplication(cloudFoundryOperations);

        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .build();
        BlueGreenConfiguration blueGreenConfiguration = BlueGreenConfiguration.builder()
                .enabled(true)
                .healthCheckEndpoint("/health")
                .build();

        when(healthChecker.assertHealthy(any(ApplicationConfiguration.class), any(BlueGreenConfiguration.class), any(logger.getClass())))
                .thenThrow(new HealthCheckException());

        // When
        boolean failed = false;
        try {

            StepVerifier.create(service.push(applicationConfiguration, testPushConfiguration, blueGreenConfiguration))
                    .expectError(HealthCheckException.class)
                    .verify();
        } catch (HealthCheckException e){
            failed = true;
        }

        // Then
        assertTrue(failed);

        ApplicationConfiguration convertedConfig = blueGreenConfiguration.convertToDarkAppConfiguration(applicationConfiguration);
        verify(healthChecker).assertHealthy(eq(convertedConfig), eq(blueGreenConfiguration), any(logger.getClass()));
        verify(cloudFoundryOperations.routes(), never()).map(any(MapRouteRequest.class));
    }

    @Test
    public void defaultDomainWhenSharedDomainsExist(){
        // Given
        String organizationName = "Test Org";
        requestOrganizationInfo(organizationName);
        requestSharedDomainsReturnMultiple();
        requestPrivateDomains();

        // When
        StepVerifier
                .create(service.defaultDomain(organizationName))
                .expectNext("cloudfoundry.org")
                .expectComplete()
                .verify();
    }

    @Test
    public void defaultDomainWhenNoSharedDomainsExist(){
        // Given
        String organizationName = "Test Org";
        requestOrganizationInfo(organizationName);
        requestSharedDomainsReturnEmpty();
        requestPrivateDomains();

        // When
        StepVerifier
                .create(service.defaultDomain(organizationName))
                .expectNext("davidehringer.com")
                .expectComplete()
                .verify();
    }

    private void requestPrivateDomains() {
        when(cloudFoundryClient
                .organizations()
                .listPrivateDomains(ListOrganizationPrivateDomainsRequest.builder()
                        .organizationId(anyString())
                        .page(1)
                        .build()))
                .thenReturn(Mono.just(ListOrganizationPrivateDomainsResponse.builder()
                        .resource(PrivateDomainResource.builder()
                                        .metadata(Metadata.builder()
                                                .id("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                                .url("/v2/private_domains/f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                                .createdAt("2015-11-30T23:38:35Z")
                                                .updatedAt(null)
                                                .build())
                                        .entity(PrivateDomainEntity.builder()
                                                .name("davidehringer.com")
                                                .owningOrganizationId("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                                .build())
                                        .build(),
                                PrivateDomainResource.builder()
                                        .metadata(Metadata.builder()
                                                .id("f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                                .url("/v2/private_domains/f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                                .createdAt("2015-11-30T23:38:35Z")
                                                .updatedAt(null)
                                                .build())
                                        .entity(PrivateDomainEntity.builder()
                                                .name("example.com")
                                                .owningOrganizationId("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                                .build())
                                        .build())
                        .totalResults(2)
                        .totalPages(1)
                        .build()));
    }

    private void requestSharedDomainsReturnEmpty() {
        when(cloudFoundryClient
                .sharedDomains()
                .list(ListSharedDomainsRequest.builder()
                        .page(1)
                        .build()))
                .thenReturn(Mono
                        .just(ListSharedDomainsResponse.builder()
                                .addAllResources(new ArrayList<>())
                                .totalResults(0)
                                .totalPages(1)
                                .build()));
    }

    private void requestSharedDomainsReturnMultiple() {
        when(cloudFoundryClient
                .sharedDomains()
                .list(ListSharedDomainsRequest.builder()
                        .page(1)
                        .build()))
                .thenReturn(Mono
                        .just(ListSharedDomainsResponse.builder()
                                .resource(SharedDomainResource.builder()
                                                .metadata(Metadata.builder()
                                                        .id("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                                        .url("/v2/shared_domains/f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                                        .createdAt("2015-11-30T23:38:35Z")
                                                        .updatedAt(null)
                                                        .build())
                                                .entity(SharedDomainEntity.builder()
                                                        .name("cloudfoundry.org")
                                                        .build())
                                                .build(),
                                        SharedDomainResource.builder()
                                                .metadata(Metadata.builder()
                                                        .id("3595f6cb-81cf-424e-a546-533877ccccfd")
                                                        .url("/v2/shared_domains/3595f6cb-81cf-424e-a546-533877ccccfd")
                                                        .createdAt("2015-11-30T23:38:35Z")
                                                        .updatedAt(null)
                                                        .build())
                                                .entity(SharedDomainEntity.builder()
                                                        .name("test.example.com")
                                                        .build())
                                                .build())
                                .build()));
    }

    private void requestOrganizationInfo(String organizationName) {
        when(cloudFoundryOperations
                .organizations()
                .get(OrganizationInfoRequest.builder()
                        .name(organizationName)
                        .build()))
                .thenReturn(Mono.just(OrganizationDetail.builder()
                        .name(organizationName)
                        .id("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                        .quota(OrganizationQuota.builder()
                                .name("test-quota")
                                .id("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                .instanceMemoryLimit(1024)
                                .organizationId("f01b174d-c750-46b0-9ddf-3aeb2064d796")
                                .paidServicePlans(true)
                                .totalMemoryLimit(1024)
                                .totalRoutes(1000)
                                .totalServiceInstances(10000)
                                .build())
                        .build()));
    }

    //TODO Update these test cases with the new GA apis for Tasks http://v3-apidocs.cloudfoundry.org/version/3.33.0/index.html

    @Test
    public void runningATaskWithMinimumParameters(){
        // Given
        String applicationId = UUID.randomUUID().toString();
        String appName = "test-app";
        String command = "run.sh";
        String taskName = "My-task";
        Integer memory = 1024;
        Map<String, String> environment = null;

        doRunATask(applicationId, appName, command, taskName, memory, environment);
    }

    @Test
    public void runningATaskWithAllParameters(){
        // Given
        String applicationId = UUID.randomUUID().toString();
        String appName = "test-app";
        String command = "run.sh";
        String taskName = "My-task";
        Integer memory = 512;
        Map<String, String> environment = new HashMap<>();
        environment.put("MY_ENVIRONMENT", "UNIT");

        doRunATask(applicationId, appName, command, taskName, memory, environment);
    }

    private void doRunATask(String applicationId, String appName, String command, String taskName, Integer memory, Map<String, String> environment) {
        requestGetApplication(cloudFoundryOperations, appName, applicationId);
        requestCreateTask(command, taskName, memory, environment);

        // When
        StepVerifier
                .create(service.runTask(appName, command, taskName, memory, environment))
                .expectNextCount(1)
                .expectComplete()
                .verify();

        // Then
        verify(cloudFoundryClient.tasks())
                .create(CreateTaskRequest.builder()
                        .applicationId(applicationId)
                        .command(command)
                        .name(taskName)
                        .memoryInMb(memory)
                        .build());
    }

    private void requestCreateTask(String command, String taskName, Integer memory, Map<String, String> environment) {
        when(cloudFoundryClient.tasks().create(any(CreateTaskRequest.class)))
                .thenReturn(Mono.just(CreateTaskResponse.builder()
                        .id("t123")
                        .name(taskName)
                        .command(command)
                        .memoryInMb(memory)
                        .state(TaskState.PENDING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
    }

    @Test
    public void waitForTaskThatHadAlreadySuccessfullyCompleted(){
        when(cloudFoundryClient.tasks().get(GetTaskRequest.builder()
                .taskId("123")
                .build()))
                .thenReturn(
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test")
                                .command("ls -la")
                                .state(TaskState.SUCCEEDED)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()));
        mockLogManagerTailing();

        StepVerifier
                .create(service.waitForTaskCompletion("my-app", "123", 20, true))
                .expectComplete()
                .verify();

        verify(cloudFoundryClient.tasks(), never()).cancel(any(CancelTaskRequest.class));
    }

    @Test
    @Ignore("The product code works. But something is not working in the subsequent return values for the Mockito 'when' call. It always returns the 1st value")
    public void waitForTaskThatSuccessfullyCompletes(){
        when(cloudFoundryClient.tasks().get(GetTaskRequest.builder()
                .taskId("123")
                .build()))
                .thenReturn(
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test1")
                                .command("ls -la")
                                .state(TaskState.RUNNING)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()),
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test 2")
                                .command("ls -la")
                                .state(TaskState.RUNNING)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()),
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test 3")
                                .command("ls -la")
                                .state(TaskState.SUCCEEDED)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()));
        when(cloudFoundryClient.tasks().cancel(any(CancelTaskRequest.class)))
                .thenReturn(Mono.just(CancelTaskResponse.builder()
                        .id("123")
                        .name("test 3")
                        .command("ls -la")
                        .state(TaskState.CANCELING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
        mockLogManagerTailing();

        StepVerifier
                .create(service.waitForTaskCompletion("my-app", "123", 20, true))
                .expectComplete()
                .verify();

        verify(cloudFoundryClient.tasks(), never()).cancel(any(CancelTaskRequest.class));
    }

    @Test
    public void waitForTaskThatHadAlreadyFailed(){
        when(cloudFoundryClient.tasks().get(GetTaskRequest.builder()
                .taskId("123")
                .build()))
                .thenReturn(
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test")
                                .command("ls -la")
                                .state(TaskState.FAILED)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()));
        when(cloudFoundryClient.tasks().cancel(any(CancelTaskRequest.class)))
                .thenReturn(Mono.just(CancelTaskResponse.builder()
                        .id("123")
                        .name("test 3")
                        .command("ls -la")
                        .state(TaskState.CANCELING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
        mockLogManagerTailing();

        StepVerifier
                .create(service.waitForTaskCompletion("my-app", "123", 20, true))
                .expectError()
                .verify();

        verify(cloudFoundryClient.tasks(), never()).cancel(any(CancelTaskRequest.class));
    }

    @Test
    @Ignore("The product code works. But something is not working in the subsequent return values for the Mockito 'when' call. It always returns the 1st value")
    public void waitForTaskThatEventuallyFails(){
        when(cloudFoundryClient.tasks().get(any(GetTaskRequest.class)))
                .thenReturn(
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test1")
                                .command("ls -la")
                                .state(TaskState.RUNNING)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()))
                .thenReturn(
                        Mono.just(GetTaskResponse.builder()
                                .id("123")
                                .name("test 2")
                                .command("ls -la")
                                .state(TaskState.RUNNING)
                                .diskInMb(1024)
                                .dropletId("droplet-id")
                                .memoryInMb(1024)
                                .sequenceId(0)
                                .createdAt("")
                                .updatedAt("")
                                .build()));
        when(cloudFoundryClient.tasks().cancel(any(CancelTaskRequest.class)))
                .thenReturn(Mono.just(CancelTaskResponse.builder()
                        .id("123")
                        .name("test 3")
                        .command("ls -la")
                        .state(TaskState.CANCELING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
        mockLogManagerTailing();

        StepVerifier
                .create(service.waitForTaskCompletion("my-app", "123", 20, true))
                .expectError()
                .verify();

        verify(cloudFoundryClient.tasks(), never()).cancel(any(CancelTaskRequest.class));
    }

    @Test
    public void waitForTaskDoNotTerminateOnTimeout(){
        when(cloudFoundryClient.tasks().get(GetTaskRequest.builder()
                .taskId("123")
                .build()))
                .thenReturn(Mono.just(GetTaskResponse.builder()
                        .id("123")
                        .name("test")
                        .command("ls -la")
                        .state(TaskState.RUNNING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
        mockLogManagerTailing();

        StepVerifier
                .create(service.waitForTaskCompletion("my-app", "123", 1, false))
                .expectError()
                .verify();

        verify(cloudFoundryClient.tasks(), never()).cancel(any(CancelTaskRequest.class));
    }

    @Test
    public void waitForTaskTerminateOnTimeout(){
        when(cloudFoundryClient.tasks().get(GetTaskRequest.builder()
                .taskId("123")
                .build()))
                .thenReturn(Mono.just(GetTaskResponse.builder()
                        .id("123")
                        .name("test")
                        .command("ls -la")
                        .state(TaskState.RUNNING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
        when(cloudFoundryClient.tasks().cancel(CancelTaskRequest.builder()
                .taskId("123")
                .build()))
                .thenReturn(Mono.just(CancelTaskResponse.builder()
                        .id("123")
                        .name("test 3")
                        .command("ls -la")
                        .state(TaskState.CANCELING)
                        .diskInMb(1024)
                        .dropletId("droplet-id")
                        .memoryInMb(1024)
                        .sequenceId(0)
                        .createdAt("")
                        .updatedAt("")
                        .build()));
        mockLogManagerTailing();

        StepVerifier
                .create(service.waitForTaskCompletion("my-app", "123", 1, true))
                .expectError()
                .verify();

        verify(cloudFoundryClient.tasks()).cancel(CancelTaskRequest.builder()
                .taskId("123")
                .build());
    }

    private void mockLogManagerTailing() {
        requestGetApplication(cloudFoundryOperations);
        when(dopplerClient.stream(any(StreamRequest.class)))
                .thenReturn(Flux.empty());
    }

    @Test
    public void startAppWithStartupAndStagingTimeouts(){
        when(cloudFoundryOperations
                .applications()
                .start(any(StartApplicationRequest.class)))
                .thenReturn(Mono.empty());

        StepVerifier
                .create(service.startApp("my-app", 500, 1500))
                .expectComplete()
                .verify();

        verify(cloudFoundryOperations.applications()).start(StartApplicationRequest.builder()
                .name("my-app")
                .startupTimeout(Duration.ofSeconds(500))
                .stagingTimeout(Duration.ofSeconds(1500))
                .build());
    }

    @Test
    public void whenBindingAServiceToARouteIfTheServiceDoesNotExistAnExceptionIsThrown(){
        when(cloudFoundryOperations
                .services()
                .getInstance(any(GetServiceInstanceRequest.class)))
            .thenReturn(Mono.empty());

        service
                .bindRouteService("my-service", "cloudfoundry.org", "docs", null, new HashMap<>())
                .as(StepVerifier::create)
                .expectError(IllegalArgumentException.class)
                .verify();
    }

    @Test
    public void whenBindingAServiceToARouteIfTheRouteDoesNotExistAnExceptionIsThrown(){
        when(cloudFoundryOperations
                .services()
                .getInstance(any(GetServiceInstanceRequest.class)))
                .thenReturn(Mono.just(ServiceInstance.builder()
                        .name("my-service")
                        .type(ServiceInstanceType.MANAGED)
                        .id(UUID.randomUUID().toString())
                        .build()));
        when(cloudFoundryOperations.domains().list())
                .thenReturn(Flux.just(Domain.builder()
                        .status(Status.SHARED)
                        .id(UUID.randomUUID().toString())
                        .name("cloudfoundry.org")
                        .build()));
        when(cloudFoundryClient
                .routes()
                .list(any(org.cloudfoundry.client.v2.routes.ListRoutesRequest.class)))
            .thenReturn(Mono.just(ListRoutesResponse.builder()
                    .addAllResources(new ArrayList<>())
                    .totalPages(1)
                    .totalResults(0)
                    .build()));

        service
                .bindRouteService("my-service", "cloudfoundry.org", "blah", null, new HashMap<>())
                .as(StepVerifier::create)
                .expectErrorMatches(routeDoesNotExistError())
                .verify();
    }

    private Predicate<Throwable> routeDoesNotExistError(){
        return throwable -> throwable instanceof IllegalArgumentException && throwable.getMessage().contains("Specified route does not exist.");
    }

    @Test
    public void whenBindingAServiceToARouteItHasAlreadyBeenBoundToAnExceptionIsThrown(){
        AccumulatingLogger accumulatingLogger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, accumulatingLogger, healthChecker);

        when(cloudFoundryOperations
                .services()
                .getInstance(any(GetServiceInstanceRequest.class)))
                .thenReturn(Mono.just(ServiceInstance.builder()
                        .name("my-service")
                        .type(ServiceInstanceType.MANAGED)
                        .id(UUID.randomUUID().toString())
                        .build()));
        when(cloudFoundryOperations.domains().list())
                .thenReturn(Flux.just(Domain.builder()
                        .status(Status.SHARED)
                        .id("d-123")
                        .name("cloudfoundry.org")
                        .build()));
        when(cloudFoundryClient
                .routes()
                .list(any(org.cloudfoundry.client.v2.routes.ListRoutesRequest.class)))
                .thenReturn(Mono.just(ListRoutesResponse.builder()
                        .addAllResources(Lists.newArrayList(RouteResource.builder()
                                .entity(RouteEntity.builder()
                                        .domainId("d-123")
                                        .host("docs")
                                        .spaceId("123")
                                        .build())
                                .metadata(Metadata.builder()
                                        .id("f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                        .url("/v2/routes/f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                        .createdAt("2015-11-30T23:38:35Z")
                                        .updatedAt(null)
                                        .build())
                                .build()))
                        .totalPages(1)
                        .totalResults(1)
                        .build()));
        when(cloudFoundryClient
                .serviceInstances()
                .bindRoute(any(BindServiceInstanceRouteRequest.class)))
                .thenThrow(new ClientV2Exception(400, 130008, "The route and service instance are already bound.", "CF-ServiceInstanceAlreadyBoundToSameRoute"));

        service
                .bindRouteService("my-service", "cloudfoundry.org", "docs", null, new HashMap<>())
                .as(StepVerifier::create)
                .expectComplete()
                .verify();

        assertTrue(accumulatingLogger.containsWarnLogEntry("The service instance is already bound to the route. No action will be taken."));
    }


    @Test
    public void successfullyBindingAServiceToARoute(){
        AccumulatingLogger accumulatingLogger = new AccumulatingLogger();
        service = new DefaultCloudFoundryService(cloudFoundryOperations, cloudFoundryClient, dopplerClient, accumulatingLogger, healthChecker);

        when(cloudFoundryOperations
                .services()
                .getInstance(any(GetServiceInstanceRequest.class)))
                .thenReturn(Mono.just(ServiceInstance.builder()
                        .name("my-service")
                        .type(ServiceInstanceType.MANAGED)
                        .id(UUID.randomUUID().toString())
                        .build()));
        when(cloudFoundryOperations.domains().list())
                .thenReturn(Flux.just(Domain.builder()
                        .status(Status.SHARED)
                        .id("d-123")
                        .name("cloudfoundry.org")
                        .build()));
        when(cloudFoundryClient
                .routes()
                .list(any(org.cloudfoundry.client.v2.routes.ListRoutesRequest.class)))
                .thenReturn(Mono.just(ListRoutesResponse.builder()
                        .addAllResources(Lists.newArrayList(RouteResource.builder()
                                .entity(RouteEntity.builder()
                                        .domainId("d-123")
                                        .host("docs")
                                        .spaceId("123")
                                        .build())
                                .metadata(Metadata.builder()
                                        .id("f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                        .url("/v2/routes/f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                        .createdAt("2015-11-30T23:38:35Z")
                                        .updatedAt(null)
                                        .build())
                                .build()))
                        .totalPages(1)
                        .totalResults(1)
                        .build()));
        when(cloudFoundryClient
                .serviceInstances()
                .bindRoute(any(BindServiceInstanceRouteRequest.class)))
                .thenReturn(Mono.just(BindServiceInstanceRouteResponse.builder()
                        .metadata(Metadata.builder()
                                .id("f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                .url("/v2/service_instances/f01b174d-c750-46b0-9ddf-3aeb2064d797/routes/f01b174d-c750-46b0-9ddf-3aeb2064d797")
                                .createdAt("2015-11-30T23:38:35Z")
                                .updatedAt(null)
                                .build())
                        .entity(ServiceInstanceEntity.builder()
                                .spaceId("123")
                                .name("my-service")
                                .build())
                        .build()));

        service
                .bindRouteService("my-service", "cloudfoundry.org", "docs", null, new HashMap<>())
                .as(StepVerifier::create)
                .expectComplete()
                .verify();

        verify(cloudFoundryClient.serviceInstances()).bindRoute(any(BindServiceInstanceRouteRequest.class));
    }

}
