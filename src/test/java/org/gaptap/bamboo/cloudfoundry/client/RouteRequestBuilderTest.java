/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;


import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.gaptap.bamboo.cloudfoundry.client.RouteRequestBuilder;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class RouteRequestBuilderTest {

    @Test
    public void hostAndDomain(){
        String route = "www.example.com";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);
        
        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example.com"));
        assertThat(request.getHost(), is("www"));
        assertThat(request.getPath(), is(nullValue()));
    }

    @Test
    public void hostWithUnderscores(){
        String route = "www_something_else.example.com";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);

        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example.com"));
        assertThat(request.getHost(), is("www_something_else"));
        assertThat(request.getPath(), is(nullValue()));
    }

    @Test
    public void domainWithUnderscores(){
        String route = "www.example_something_else.com";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);

        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example_something_else.com"));
        assertThat(request.getHost(), is("www"));
        assertThat(request.getPath(), is(nullValue()));
    }

    @Test
    public void hostDomainAndPath(){
        String route = "some-thing.example.com/my-app";

        MapRouteRequest request = RouteRequestBuilder.buildMapRouteRequest("my-app", route);

        assertThat(request.getApplicationName(), is("my-app"));
        assertThat(request.getDomain(), is("example.com"));
        assertThat(request.getHost(), is("some-thing"));
        assertThat(request.getPath(), is("/my-app"));
    }

    @Test
    public void toStringMapRouteRequestWithOnlyDomain(){
        MapRouteRequest request = MapRouteRequest.builder()
                .applicationName("my-app")
                .domain("cf.com")
                .build();

        assertThat(RouteRequestBuilder.toString(request), is("cf.com"));
    }

    @Test
    public void toStringMapRouteRequestWithDomainAndHost(){
        MapRouteRequest request = MapRouteRequest.builder()
                .applicationName("my-app")
                .domain("cf.com")
                .host("host")
                .build();

        assertThat(RouteRequestBuilder.toString(request), is("host.cf.com"));
    }

    @Test
    public void toStringMapRouteRequestWithDomainHostAndPath(){
        MapRouteRequest request = MapRouteRequest.builder()
                .applicationName("my-app")
                .domain("cf.com")
                .host("host")
                .path("/some-path")
                .build();

        assertThat(RouteRequestBuilder.toString(request), is("host.cf.com/some-path"));
    }

    @Test
    public void convertApplicationConfigurationWithHostsAndDomainsToMapRouteRequests(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .domain("domain-one.com", "domain-two.com")
                .host("host-one", "host-two")
                .build();

        List<MapRouteRequest> request = RouteRequestBuilder.buildMapRouteRequests(applicationConfiguration);

        assertThat(request.size(), is(4));
        assertThat(request.get(0).getApplicationName(), is(applicationConfiguration.name()));

        assertThat(request.get(0).getHost(), is("host-one"));
        assertThat(request.get(0).getDomain(), is("domain-one.com"));

        assertThat(request.get(1).getHost(), is("host-two"));
        assertThat(request.get(1).getDomain(), is("domain-one.com"));

        assertThat(request.get(2).getHost(), is("host-one"));
        assertThat(request.get(2).getDomain(), is("domain-two.com"));

        assertThat(request.get(3).getHost(), is("host-two"));
        assertThat(request.get(3).getDomain(), is("domain-two.com"));
    }

    @Test
    public void convertApplicationConfigurationWithNoHostnameToMapRouteRequests(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .domain("domain-one.com", "domain-two.com")
                .noHostname(true)
                .build();

        List<MapRouteRequest> request = RouteRequestBuilder.buildMapRouteRequests(applicationConfiguration);

        assertThat(request.size(), is(2));
        assertThat(request.get(0).getApplicationName(), is(applicationConfiguration.name()));

        assertThat(request.get(0).getDomain(), is("domain-one.com"));
        assertThat(request.get(1).getDomain(), is("domain-two.com"));
    }

    @Test
    public void convertApplicationConfigurationWithNoHostnameSetToFalseButNotHostsSpecifiedToMapRouteRequests(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .domain("domain-one.com", "domain-two.com")
                .noHostname(false)
                .build();

        List<MapRouteRequest> request = RouteRequestBuilder.buildMapRouteRequests(applicationConfiguration);

        assertThat(request.size(), is(2));
        assertThat(request.get(0).getApplicationName(), is(applicationConfiguration.name()));

        assertThat(request.get(0).getHost(), is("test-app"));
        assertThat(request.get(0).getDomain(), is("domain-one.com"));

        assertThat(request.get(1).getHost(), is("test-app"));
        assertThat(request.get(1).getDomain(), is("domain-two.com"));
    }

    @Test
    public void convertApplicationConfigurationWithRoutesToMapRouteRequests(){
        ApplicationConfiguration applicationConfiguration = ApplicationConfiguration.builder()
                .name("test-app")
                .route("host-one.domain-one.com", "host-two.domain-two.com/some-path")
                .build();

        List<MapRouteRequest> request = RouteRequestBuilder.buildMapRouteRequests(applicationConfiguration);

        assertThat(request.size(), is(2));
        assertThat(request.get(0).getApplicationName(), is(applicationConfiguration.name()));

        assertThat(request.get(0).getHost(), is("host-one"));
        assertThat(request.get(0).getDomain(), is("domain-one.com"));
        assertThat(request.get(0).getPath(), is(nullValue()));

        assertThat(request.get(1).getHost(), is("host-two"));
        assertThat(request.get(1).getDomain(), is("domain-two.com"));
        assertThat(request.get(1).getPath(), is("/some-path"));
    }

}
