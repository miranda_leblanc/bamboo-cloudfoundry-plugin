/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import org.gaptap.bamboo.cloudfoundry.cli.CliFormatter;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryService;
import org.jetbrains.annotations.NotNull;

import java.util.concurrent.CountDownLatch;

import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.BIND_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.BIND_SERVICE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_FAIL_IF_EXISTS;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_INSTANCE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_SERVICE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.CREATE_SERVICE_PLAN;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.DELETE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_BIND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_CREATE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_DELETE;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_LIST;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_SHOW;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.OPTION_UNBIND;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.SELECTED_OPTION;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.SHOW_SERVICE_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.UNBIND_APP_NAME;
import static org.gaptap.bamboo.cloudfoundry.tasks.config.ServiceTaskConfigurator.UNBIND_SERVICE_NAME;

/**
 * @author David Ehringer
 */
public class ServiceTask extends AbstractCloudFoundryTask {

    public ServiceTask(EncryptionService encryptionService) {
        super(encryptionService);
    }

    @Override
    @NotNull
    public TaskResult doExecute(@NotNull CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        ConfigurationMap configMap = taskContext.getConfigurationMap();
        String option = configMap.get(SELECTED_OPTION);

        CloudFoundryService cloudFoundry = getCloudFoundryService(taskContext);

        TaskResultBuilder taskResultBuilder = TaskResultBuilder.newBuilder(taskContext).success();
        try {
            if (OPTION_LIST.equals(option)) {
                buildLogger.addBuildLogEntry("Getting services " + getLoginContext(taskContext));
                list(cloudFoundry, buildLogger, taskResultBuilder);
            } else if (OPTION_SHOW.equals(option)) {
                buildLogger.addBuildLogEntry("Showing service " + configMap.get(SHOW_SERVICE_NAME) + " " + getLoginContext(taskContext));
                show(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_CREATE.equals(option)) {
                buildLogger.addBuildLogEntry("Creating service " + configMap.get(CREATE_INSTANCE_NAME) + " " + getLoginContext(taskContext));
                create(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_DELETE.equals(option)) {
                buildLogger.addBuildLogEntry("Deleting service " + configMap.get(DELETE_NAME) + " " + getLoginContext(taskContext));
                delete(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_BIND.equals(option)) {
                buildLogger.addBuildLogEntry("Binding service " + configMap.get(BIND_SERVICE_NAME) + " to app " + configMap.get(BIND_APP_NAME) + " " + getLoginContext(taskContext));
                bind(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else if (OPTION_UNBIND.equals(option)) {
                buildLogger.addBuildLogEntry("Unbinding app " + configMap.get(UNBIND_APP_NAME) + " from service " + configMap.get(UNBIND_SERVICE_NAME) + " " + getLoginContext(taskContext));
                unbind(cloudFoundry, buildLogger, configMap, taskResultBuilder);
            } else {
                throw new TaskException("Unknown or unspecified service managment option: " + option);
            }
        } catch (InterruptedException e) {
            buildLogger.addErrorLogEntry("Unable to complete task due to unknown error: " + e.getMessage());
            taskResultBuilder.failedWithError();
        }

        return taskResultBuilder.build();
    }

    private void list(CloudFoundryService cloudFoundry, BuildLogger buildLogger, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        CliFormatter cliFormatter = new CliFormatter(buildLogger);
        cloudFoundry.services()
                .collectList()
                .subscribe(serviceInstances -> {
                            cliFormatter.cfServices(serviceInstances);
                        },
                        throwable -> {
                            buildLogger.addErrorLogEntry("Unable to show services: " + throwable.getMessage());
                            taskResultBuilder.failedWithError();
                            latch.countDown();
                        },
                        latch::countDown);
        latch.await();
    }

    private void show(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String serviceName = configMap.get(SHOW_SERVICE_NAME);
        CountDownLatch latch = new CountDownLatch(1);
        CliFormatter cliFormatter = new CliFormatter(buildLogger);
        cloudFoundry.service(serviceName)
                .subscribe(serviceInstance -> {
                            cliFormatter.cfService(serviceInstance);
                        },
                        throwable -> {
                            buildLogger.addErrorLogEntry("Unable to show service: " + throwable.getMessage());
                                    taskResultBuilder.failedWithError();
                            latch.countDown();
                        },
                        latch::countDown);
        latch.await();

    }

    private void create(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(CREATE_INSTANCE_NAME);
        String label = configMap.get(CREATE_SERVICE_NAME);
        String plan = configMap.get(CREATE_SERVICE_PLAN);
        String failIfExists = configMap.get(CREATE_FAIL_IF_EXISTS);
        doSubscribe(cloudFoundry.createService(name, label, plan, Boolean.valueOf(failIfExists)), "Unable to create service", buildLogger, taskResultBuilder);
    }

    private void delete(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String name = configMap.get(DELETE_NAME);
        doSubscribe(cloudFoundry.deleteService(name), "Unable to delete service", buildLogger, taskResultBuilder);
    }

    private void bind(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String serviceName = configMap.get(BIND_SERVICE_NAME);
        String applicationName = configMap.get(BIND_APP_NAME);
        doSubscribe(cloudFoundry.bindService(serviceName, applicationName), "Unable to bind service to app", buildLogger, taskResultBuilder);
    }

    private void unbind(CloudFoundryService cloudFoundry, BuildLogger buildLogger, ConfigurationMap configMap, TaskResultBuilder taskResultBuilder) throws InterruptedException {
        String serviceName = configMap.get(UNBIND_SERVICE_NAME);
        String applicationName = configMap.get(UNBIND_APP_NAME);
        doSubscribe(cloudFoundry.unbindService(serviceName, applicationName), "Unable to unbind service from app", buildLogger, taskResultBuilder);
    }

}
