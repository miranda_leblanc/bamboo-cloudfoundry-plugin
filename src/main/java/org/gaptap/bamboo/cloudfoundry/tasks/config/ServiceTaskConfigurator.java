/*
 * Copyright 2013 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.config;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.security.EncryptionService;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.struts.TextProvider;
import com.google.common.collect.ImmutableList;
import org.gaptap.bamboo.cloudfoundry.admin.CloudFoundryAdminService;
import org.gaptap.bamboo.cloudfoundry.client.CloudFoundryServiceFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author David Ehringer
 */
public class ServiceTaskConfigurator extends BaseCloudFoundryTaskConfigurator {

    public static final String OPTIONS = "cf_options";
    public static final String SELECTED_OPTION = "cf_option";
    public static final String OPTION_BIND = "bind";
    public static final String OPTION_UNBIND = "unbind";
    public static final String OPTION_CREATE = "create";
    public static final String OPTION_DELETE = "delete";
    public static final String OPTION_LIST = "list";
    public static final String OPTION_SHOW = "show";

    public static final String CREATE_SERVICE_NAME = "cf_createLabel";
    public static final String CREATE_SERVICE_PLAN = "cf_createPlan";
    public static final String CREATE_INSTANCE_NAME = "cf_createName";
    public static final String CREATE_FAIL_IF_EXISTS = "cf_createFailIfExists";

    public static final String SHOW_SERVICE_NAME = "cf_showServiceName";
    public static final String DELETE_NAME = "cf_deleteName";
    public static final String BIND_SERVICE_NAME = "cf_bindServiceName";
    public static final String BIND_APP_NAME = "cf_bindAppName";
    public static final String UNBIND_SERVICE_NAME = "cf_unbindServiceName";
    public static final String UNBIND_APP_NAME = "cf_unbindAppName";

    private static final String DEFAULT_CREATE_FAIL_IF_EXISTS = "false";

    private static final String SERVICE_CONFIG_SEPARATOR = ":_:";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_OPTION, SHOW_SERVICE_NAME,
            CREATE_INSTANCE_NAME, CREATE_SERVICE_NAME, CREATE_SERVICE_PLAN, CREATE_FAIL_IF_EXISTS, DELETE_NAME, BIND_SERVICE_NAME, BIND_APP_NAME, UNBIND_SERVICE_NAME,
            UNBIND_APP_NAME);

    public ServiceTaskConfigurator(CloudFoundryAdminService adminService,
                                   TextProvider textProvider,
                                   TaskConfiguratorHelper taskConfiguratorHelper,
                                   EncryptionService encryptionService,
                                   CloudFoundryServiceFactory cloudFoundryServiceFactory) {
        super(adminService, textProvider, taskConfiguratorHelper, encryptionService, cloudFoundryServiceFactory);
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
            @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> configMap = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(configMap, params, FIELDS_TO_COPY);
        return configMap;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(CREATE_FAIL_IF_EXISTS, DEFAULT_CREATE_FAIL_IF_EXISTS);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        populateContextForAll(context);
        populateContextForModify(context, taskDefinition);
    }

    private void populateContextForModify(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> options = new LinkedHashMap<String, String>();
        options.put(OPTION_BIND, textProvider.getText("cloudfoundry.task.service.option.bind"));
        options.put(OPTION_UNBIND, textProvider.getText("cloudfoundry.task.service.option.unbind"));
        options.put(OPTION_CREATE, textProvider.getText("cloudfoundry.task.service.option.create"));
        options.put(OPTION_DELETE, textProvider.getText("cloudfoundry.task.service.option.delete"));
        options.put(OPTION_LIST, textProvider.getText("cloudfoundry.task.service.option.list"));
        options.put(OPTION_SHOW, textProvider.getText("cloudfoundry.task.service.option.show"));
        context.put(OPTIONS, options);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String option = params.getString(SELECTED_OPTION);

        if (OPTION_SHOW.equals(option)) {
            validateRequiredNotBlank(SHOW_SERVICE_NAME, params, errorCollection);
        } else if (OPTION_CREATE.equals(option)) {
            validateRequiredNotBlank(CREATE_INSTANCE_NAME, params, errorCollection);
            validateRequiredNotBlank(CREATE_SERVICE_NAME, params, errorCollection);
            validateRequiredNotBlank(CREATE_SERVICE_PLAN, params, errorCollection);
        } else if (OPTION_DELETE.equals(option)) {
            validateRequiredNotBlank(DELETE_NAME, params, errorCollection);
        } else if (OPTION_BIND.equals(option)) {
            validateRequiredNotBlank(BIND_APP_NAME, params, errorCollection);
            validateRequiredNotBlank(BIND_SERVICE_NAME, params, errorCollection);
        } else if (OPTION_UNBIND.equals(option)) {
            validateRequiredNotBlank(UNBIND_APP_NAME, params, errorCollection);
            validateRequiredNotBlank(UNBIND_SERVICE_NAME, params, errorCollection);
        }
    }
}
