/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.tasks.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.gaptap.bamboo.cloudfoundry.client.ApplicationConfiguration;
import org.gaptap.bamboo.cloudfoundry.client.ServiceConfiguration;
import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author David Ehringer
 */
public class YamlToServiceConfigurationMapper {

    private ObjectMapper objectMapper = new ObjectMapper();

    public List<ServiceConfiguration> from(InputStream input){
        List<ServiceConfiguration> services = new ArrayList<>();
        List<Map<String, Object>> serviceMaps = getServices(input);
        for(Map<String, Object> serviceMap: serviceMaps){
            ServiceConfiguration serviceConfiguration = ServiceConfiguration.builder()
                    .service((String)serviceMap.get("service"))
                    .serviceInstance((String)serviceMap.get("service_instance"))
                    .plan((String)serviceMap.get("plan"))
                    .tags(getTags(serviceMap))
                    .parameters(getParameters(serviceMap))
                    .build();
            services.add(serviceConfiguration);
        }
        return services;
    }

    private List<Map<String, Object>> getServices(InputStream input) {
        // TODO validation
        return (List<Map<String, Object>>)readManifest(input).get("services");
    }

    private Map readManifest(InputStream input) {
        Yaml yaml = new Yaml();
        return (Map) yaml.load(input);
    }

    private List<String> getTags(Map<String, Object> serviceMap) {
        return (List<String>)serviceMap.get("tags");
    }

    private Map<String,Object> getParameters(Map<String, Object> serviceMap) {
        Object params = serviceMap.get("parameters_json");
        if(params != null && params instanceof String){
            return getParametersJson((String) params);
        }

        params = serviceMap.get("parameters");
        if(params != null && params instanceof Map){
            return (Map<String, Object>) params;
        }

        // TODO error
        return new HashMap<>();
    }

    private Map<String, Object> getParametersJson(String json){
        try {
            return objectMapper.readValue(json, Map.class);
        } catch (IOException e) {
            e.printStackTrace();
            // TODO
            return new HashMap<>();
        }
    }

}
