package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.reactor.util.DefaultSslCertificateTruster;
import org.cloudfoundry.reactor.util.SslCertificateTruster;
import org.cloudfoundry.reactor.util.StaticTrustManagerFactory;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;
import reactor.ipc.netty.http.client.HttpClient;
import reactor.ipc.netty.http.client.HttpClientResponse;

import java.time.Duration;
import java.util.Optional;
import java.util.concurrent.CountDownLatch;

public class ReactorHealthChecker implements HealthChecker {

    private HttpClient client;
    private final SslCertificateTruster truster;

    public ReactorHealthChecker() {
        truster =  new DefaultSslCertificateTruster(Optional.empty());
        client = HttpClient.create(options -> {
            options.sslSupport(ssl -> {
                ssl.trustManager(new StaticTrustManagerFactory(truster));
            });
        });
    }

    public void setHttpClient(HttpClient httpClient) {
        this.client = httpClient;
    }

    @Override
    public Mono<Void> assertHealthy(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration, Logger logger) {
        assertRoutesMapped(applicationConfiguration);

        UriComponents uriComponents = getUriComponents(applicationConfiguration, blueGreenConfiguration);

        configureSsl(blueGreenConfiguration, uriComponents);

        return client
                .get(uriComponents.toString())
                .doOnSubscribe(subscription -> logger.info("Checking health of " + uriComponents.toUriString()))
                .doOnError(throwable -> logger.error("Health check failed: " + throwable.getMessage()))
                .map(httpClientResponse -> assertSuccess(httpClientResponse, logger))
                .then();
    }

    protected UriComponents getUriComponents(ApplicationConfiguration applicationConfiguration, BlueGreenConfiguration blueGreenConfiguration) {
        UriComponents originalUri = UriComponentsBuilder
                .fromUriString("//" + applicationConfiguration.routes().get(0))
                .build();

        return UriComponentsBuilder.newInstance()
                .scheme("https")
                .host(originalUri.getHost())
                .port(443)
                .path(originalUri.getPath())
                .path(blueGreenConfiguration.healthCheckEndpoint())
                .build();
    }

    private void assertRoutesMapped(ApplicationConfiguration applicationConfiguration) {
        if(applicationConfiguration.routes().isEmpty()){
            throw new IllegalArgumentException("A health check cannot be performed on an application with no mapped routes.");
        }
    }

    private void configureSsl(BlueGreenConfiguration blueGreenConfiguration, UriComponents uriComponents) {
        if(blueGreenConfiguration.skipSslValidation()) {
            truster.trust(uriComponents.getHost(), uriComponents.getPort(), Duration.ofSeconds(30));
        }
    }

    private int assertSuccess(HttpClientResponse httpClientResponse, Logger logger){
        int responseCode = httpClientResponse.status().code();
        if(200 != responseCode){
            logger.error("Health check failed. HTTP response code: " + responseCode);
            CountDownLatch latch = new CountDownLatch(1);
            httpClientResponse
                    .receiveContent()
                    .subscribe(httpContent -> {
                        logger.error(httpContent.content().toString());
                    }
                    , throwable -> {
                        logger.error("Error occurred attempting to log content of response");
                        latch.countDown();;
                    }
                    , latch::countDown);
            try {
                latch.await();
            } catch (InterruptedException e) {
                // Nothing we can do
            }
            throw new HealthCheckException();
        }
        logger.info("Health check succeeded.");
        return responseCode;
    }
}
