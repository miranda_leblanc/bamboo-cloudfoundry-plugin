/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gaptap.bamboo.cloudfoundry.client;

import org.cloudfoundry.client.CloudFoundryClient;
import org.cloudfoundry.client.v2.applications.ListApplicationServiceBindingsRequest;
import org.cloudfoundry.client.v2.servicebindings.ServiceBindingResource;
import org.cloudfoundry.client.v2.serviceinstances.GetServiceInstanceRequest;
import org.cloudfoundry.client.v2.serviceinstances.GetServiceInstanceResponse;
import org.cloudfoundry.doppler.DopplerClient;
import org.cloudfoundry.operations.CloudFoundryOperations;
import org.cloudfoundry.operations.applications.ApplicationDetail;
import org.cloudfoundry.operations.applications.ApplicationHealthCheck;
import org.cloudfoundry.operations.applications.GetApplicationEnvironmentsRequest;
import org.cloudfoundry.operations.applications.PushApplicationRequest;
import org.cloudfoundry.operations.applications.SetEnvironmentVariableApplicationRequest;
import org.cloudfoundry.operations.applications.UnsetEnvironmentVariableApplicationRequest;
import org.cloudfoundry.operations.routes.Level;
import org.cloudfoundry.operations.routes.ListRoutesRequest;
import org.cloudfoundry.operations.routes.MapRouteRequest;
import org.cloudfoundry.operations.routes.Route;
import org.cloudfoundry.operations.routes.UnmapRouteRequest;
import org.cloudfoundry.util.PaginationUtils;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.time.Duration;
import java.util.List;
import java.util.Map;

public class DeclarativePush {

    private final CloudFoundryOperations cloudFoundryOperations;
    private final CloudFoundryClient cloudFoundryClient;
    private final DopplerClient dopplerClient;

    private final Logger logger;
    private final CloudFoundryService cloudFoundryService;

    public DeclarativePush(CloudFoundryOperations cloudFoundryOperations, CloudFoundryClient cloudFoundryClient,
                           DopplerClient dopplerClient, Logger logger, CloudFoundryService cloudFoundryService) {
        this.cloudFoundryOperations = cloudFoundryOperations;
        this.cloudFoundryClient = cloudFoundryClient;
        this.dopplerClient = dopplerClient;
        this.logger = logger;
        this.cloudFoundryService = cloudFoundryService;
    }

    public Mono<ApplicationDetail> push(ApplicationConfiguration applicationConfiguration, PushConfiguration pushConfiguration){
        String applicationName = applicationConfiguration.name();
        return cloudFoundryOperations
                .applications()
                .push(buildPushApplicationRequest(applicationConfiguration, pushConfiguration))
                .doOnSubscribe(subscription -> logPushStart(applicationName, pushConfiguration.applicationFile()))
                .doOnSuccess(it -> logPushSuccessful())
                .doOnError(e -> logPushFailed(e))
                .thenMany(bindServices(applicationConfiguration))
                .thenMany(unbindOrphanedServices(applicationConfiguration))
                .thenMany(setEnvironmentVariables(applicationConfiguration))
                .thenMany(unsetStaleEnvironmentVariables(applicationConfiguration, pushConfiguration))
                .thenMany(mapRoutes(applicationConfiguration))
                .thenMany(unmapStaleRoutes(applicationConfiguration, pushConfiguration))
                .then()
                .then(optionallyStartApp(applicationName, pushConfiguration));
    }

    private void logPushStart(String applicationName, File application) {
        logger.info("Creating/updating app " + applicationName + "...");
        logger.info("Uploading " + applicationName + "...");
        logger.info("Uploading app files from: " + application.getAbsolutePath());
    }

    private void logPushSuccessful() {
        logger.info("Done uploading");
    }

    private void logPushFailed(Throwable e) {
        logger.error("Unable to create/update and upload app: " + e.getMessage());
    }

    private PushApplicationRequest buildPushApplicationRequest(ApplicationConfiguration applicationConfiguration, PushConfiguration pushConfiguration) {
        PushApplicationRequest.Builder builder =  PushApplicationRequest.builder()
                .name(applicationConfiguration.name())
                .buildpack(applicationConfiguration.buildpackUrl())
                .command(applicationConfiguration.command())
                .path(pushConfiguration.applicationFile().toPath())
                .diskQuota(applicationConfiguration.diskQuota())
                .instances(applicationConfiguration.instances())
                .memory(applicationConfiguration.memory())
                .stack(applicationConfiguration.stack())
                .noHostname(applicationConfiguration.noHostname())
                .noStart(true)
                .timeout(applicationConfiguration.healthCheckTimeout());

        if (applicationConfiguration.noRoute() != null){
            builder.noRoute(applicationConfiguration.noRoute());
        } else {
            builder.noRoute(true);
        }

        String healthCheckType = applicationConfiguration.healthCheckType();
        if(healthCheckType != null){
            if("none".equals(healthCheckType)){
                builder.healthCheckType(ApplicationHealthCheck.NONE);
            } else if("port".equals(healthCheckType)){
                builder.healthCheckType(ApplicationHealthCheck.PORT);
            } else {
                throw new IllegalArgumentException("Unknown health-check-type: " + healthCheckType + ". Please see https://docs.cloudfoundry.org/devguide/deploy-apps/manifest.html#health-check-type.");
            }
        }

        if(pushConfiguration.startupTimeout() != null){
            builder.startupTimeout(Duration.ofSeconds(pushConfiguration.startupTimeout()));
        }
        if(pushConfiguration.stagingTimeout() != null){
            builder.stagingTimeout(Duration.ofSeconds(pushConfiguration.stagingTimeout()));
        }

        return builder.build();
    }

    private Mono<GetServiceInstanceResponse> getServiceInstance(ServiceBindingResource serviceBindingResource) {
        return cloudFoundryClient
                .serviceInstances()
                .get(GetServiceInstanceRequest.builder()
                        .serviceInstanceId(serviceBindingResource.getEntity().getServiceInstanceId())
                        .build());
    }

    private Mono<String> getApplicationId(String name){
        return cloudFoundryService.getApp(name)
                .map(applicationDetail -> applicationDetail.getId());
    }

    private Flux<ServiceBindingResource> listApplicationServiceBindings(String applicationId){
        return PaginationUtils
                .requestClientV2Resources(page -> cloudFoundryClient
                        .applicationsV2()
                        .listServiceBindings(ListApplicationServiceBindingsRequest.builder()
                                .applicationId(applicationId)
                                .build()));
    }

    private Flux<Void> bindServices(ApplicationConfiguration applicationConfiguration){
        return bindServices(applicationConfiguration.name(), applicationConfiguration.serviceBindings());
    }

    private Flux<Void> bindServices(String applicationName, List<String> serviceInstances) {
        return Flux.fromIterable(serviceInstances)
                .flatMap(serviceInstance -> cloudFoundryService.bindService(serviceInstance, applicationName));
    }

    private Flux<Void> unbindOrphanedServices(ApplicationConfiguration applicationConfiguration){
        return unbindOrphanedServices(applicationConfiguration.name(), applicationConfiguration.serviceBindings());
    }

    private Flux<Void> unbindOrphanedServices(String applicationName, List<String> desiredServiceInstances) {
        return getApplicationId(applicationName)
                .flatMap(this::listApplicationServiceBindings)
                .flatMap(this::getServiceInstance)
                .map(serviceInstanceResponse -> serviceInstanceResponse.getEntity())
                .filter(serviceInstanceEntity -> !desiredServiceInstances.contains(serviceInstanceEntity.getName()))
                .flatMap(serviceInstanceEntity -> cloudFoundryService.unbindService(serviceInstanceEntity.getName(), applicationName));
    }

    private Flux<Void> setEnvironmentVariables(ApplicationConfiguration applicationConfiguration){
        return setEnvironmentVariables(applicationConfiguration.name(), applicationConfiguration.environment());
    }

    private Flux<Void> setEnvironmentVariables(String name, Map<String, String> environmentVariables) {
        // The concurrency of 1 is very important here
        return Flux.fromIterable(environmentVariables.entrySet())
                .flatMap(entry -> setEnvironmentVariable(name, entry.getKey(), entry.getValue()), 1);
    }

    private Mono<Void> setEnvironmentVariable(String name, String key, String value) {
        return cloudFoundryOperations.applications()
                .setEnvironmentVariable(SetEnvironmentVariableApplicationRequest.builder()
                        .name(name)
                        .variableName(key)
                        .variableValue(value)
                        .build())
                .doOnSubscribe(it -> logger.info(String.format("Setting environment variable %s=[REDACTED]...", key)))
                .doOnSuccess(it -> logger.info(String.format("Setting environment variable %s=[REDACTED]... OK", key)))
                .doOnError(t -> logger.error("Unable to set environment variable " + key));
    }

    private Flux<Void> unsetStaleEnvironmentVariables(ApplicationConfiguration applicationConfiguration, PushConfiguration pushConfiguration){
        if(pushConfiguration.disableUnsettingStaleEnvVars()){
            logger.info("Unsetting of stale environment variables is disabled.");
            return Flux.empty();
        }
        return unsetStaleEnvironmentVariables(applicationConfiguration.name(), applicationConfiguration.environment());
    }

    private Flux<Void> unsetStaleEnvironmentVariables(String applicationName, Map<String, String> desiredEnvironmentVariables) {
        return cloudFoundryOperations
                .applications()
                .getEnvironments(GetApplicationEnvironmentsRequest.builder()
                        .name(applicationName)
                        .build())
                .flatMap(applicationEnvironments -> Flux.fromIterable(applicationEnvironments.getUserProvided().entrySet()))
                .filter(entry -> !desiredEnvironmentVariables.containsKey(entry.getKey()))
                .doOnNext(entry -> logger.info(String.format("Unsetting environment variable %s=[REDACTED]... OK", entry.getKey())))
                .flatMap(entry -> cloudFoundryOperations
                        .applications()
                        .unsetEnvironmentVariable(UnsetEnvironmentVariableApplicationRequest.builder()
                                .name(applicationName)
                                .variableName(entry.getKey())
                                .build()), 1);
    }

    private Flux<Void> mapRoutes(ApplicationConfiguration applicationConfiguration){
        return Flux.fromIterable(RouteRequestBuilder.buildMapRouteRequests(applicationConfiguration))
                .flatMap(request -> cloudFoundryService.map(request), 1);
    }

    private Flux<Void> unmapStaleRoutes(ApplicationConfiguration applicationConfiguration, PushConfiguration pushConfiguration){
        if(pushConfiguration.disableUnmappingStaleRoutes()){
            logger.info("Unmapping of stale routes is disabled.");
            return Flux
                    .empty();
        }
        return unmapStaleRoutes(applicationConfiguration.name(), RouteRequestBuilder.buildMapRouteRequests(applicationConfiguration));
    }

    private Flux<Void> unmapStaleRoutes(String applicationName, List<MapRouteRequest> desiredRoutes) {
        return cloudFoundryOperations
                .routes()
                .list(ListRoutesRequest.builder()
                        .level(Level.SPACE)
                        .build())
                .filter(route -> route.getApplications().contains(applicationName))
                .filter(route -> isUndesiredRoute(route, desiredRoutes))
                .map(route -> toUnmapRouteRequest(route, applicationName))
                .flatMap(request -> cloudFoundryService.unmap(request), 1);
    }

    private boolean isUndesiredRoute(Route route, List<MapRouteRequest> desiredRoutes) {
        for(MapRouteRequest request: desiredRoutes){
            if(route.getDomain().equals(request.getDomain())
                    && route.getApplications().contains(request.getApplicationName())
                    && hostsEqual(route, request)
                    && pathsEqual(route, request)){
                return false;
            }
        }
        return true;
    }

    private boolean hostsEqual(Route route, MapRouteRequest request) {
        return (route.getHost().equals("") && request.getHost() == null)
                || route.getHost().equals(request.getHost());
    }

    private boolean pathsEqual(Route route, MapRouteRequest request) {
        return (route.getPath().equals("") && request.getPath() == null)
                || route.getPath().equals(request.getPath());
    }

    private static UnmapRouteRequest toUnmapRouteRequest(Route route, String applicationName){
        return UnmapRouteRequest.builder()
                .applicationName(applicationName)
                .domain(route.getDomain())
                .host(route.getHost().equals("") ? null : route.getHost())
                .path(route.getPath().equals("") ? null : route.getPath())
                .build();
    }

    private Mono<ApplicationDetail> optionallyStartApp(String applicationName, PushConfiguration pushConfiguration) {
        if(pushConfiguration.start()){
            AppLogManager logManager = new AppLogManager(cloudFoundryOperations, dopplerClient);
            return Mono.empty()
                    .doOnSuccess(it -> logManager.startTailingLogs(applicationName, logger))
                    .then(cloudFoundryService.startApp(applicationName, pushConfiguration.startupTimeout(), pushConfiguration.stagingTimeout()))
                    .doOnTerminate((aVoid, e) -> logManager.stopTailingLogs())
                    .then(cloudFoundryService.app(applicationName));
        }
        return cloudFoundryService.app(applicationName)
                .doOnSuccess(it -> logger.info("--no-start option selected. App will not be started"));
    }
}
