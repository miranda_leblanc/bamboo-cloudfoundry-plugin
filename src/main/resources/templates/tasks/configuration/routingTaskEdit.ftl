[#import "/lib/ace.ftl" as ace ]

[#include "selectTargetFragment.ftl"]

[@ui.bambooSection titleKey='cloudfoundry.task.routing.section']

	[@ww.select labelKey="cloudfoundry.task.routing.option" name="cf_option" list=cf_options listKey="key" listValue="value" toggle='true' /]
	[@ui.bambooSection dependsOn='cf_option' showOn='addDomain']
		<p>[@ww.text name='cloudfoundry.task.routing.addDomain.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_addDomain' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='deleteDomain']
		<p>[@ww.text name='cloudfoundry.task.routing.deleteDomain.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_deleteDomain' required='true' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='addRoute']
		<p>[@ww.text name='cloudfoundry.task.routing.addRoute.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.host' name='cf_host_addRoute' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_domain_addRoute' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.path' descriptionKey='cloudfoundry.task.routing.path.description' name='cf_path_addRoute' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='deleteRoute']
		<p>[@ww.text name='cloudfoundry.task.routing.deleteRoute.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.host' name='cf_host_deleteRoute' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_domain_deleteRoute' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.path' descriptionKey='cloudfoundry.task.routing.path.description' name='cf_path_deleteRoute' /]
	[/@ui.bambooSection]
	[@ui.bambooSection dependsOn='cf_option' showOn='bindRouteService']
		<p>[@ww.text name='cloudfoundry.task.routing.bindRouteService.description' /]</p>
		[@ww.textfield labelKey='cloudfoundry.task.routing.serviceName' name='cf_serviceName_bindRouteService' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.domain' name='cf_domain_bindRouteService' required='true' /]
		[@ww.textfield labelKey='cloudfoundry.task.routing.host' name='cf_host_bindRouteService'/]
		[@ww.textfield labelKey='cloudfoundry.task.routing.path' name='cf_path_bindRouteService' /]

		[@ww.select labelKey='cloudfoundry.task.routing.bindRouteService.data.options' name='cf_dataOption_bindRouteService'
												listKey='key' listValue='value' toggle='true'
												list=dataOptionsBindRouteService /]
		[@ui.bambooSection dependsOn="cf_dataOption_bindRouteService" showOn="inline"]
			[@ace.textarea labelKey='cloudfoundry.task.routing.bindRouteService.data.inline' name="cf_inlineData_bindRouteService"/]
		[/@ui.bambooSection]
		[@ui.bambooSection dependsOn="cf_dataOption_bindRouteService" showOn="file"]
			[@ww.textfield labelKey='cloudfoundry.task.routing.bindRouteService.data.file' name='cf_file_bindRouteService' cssClass="long-field" /]
		[/@ui.bambooSection]
	[/@ui.bambooSection]
	
[/@ui.bambooSection]