

<html>
<head>
	[@ui.header pageKey="cloudfoundry.global.targets.title" title=true /]
	<meta name="decorator" content="adminpage">
    ${webResourceManager.requireResource("org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloudfoundry-plugin-resources")}
</head>
<body>
	[@ui.header pageKey="cloudfoundry.global.configuration.heading" /]
	<p>
		<img src="${req.contextPath}/download/resources/org.gaptap.bamboo.cloudfoundry.cloudfoundry-plugin:cloudfoundry-plugin-resources/images/CloudFoundryCorp_vertical_RGB-icon.png" style="float: left; margin-right: 5px" width="80" height="49" />[@ww.text name='cloudfoundry.global.configuration.description' /]
	</p>
	[@ww.actionmessage /]
	[@ui.clear/]
	
	[@dj.tabContainer headingKeys=["cloudfoundry.targets.tab.heading", "cloudfoundry.credentials.tab.heading", "cloudfoundry.proxies.tab.heading"] selectedTab='${selectedTab!}']
	    [@dj.contentPane labelKey="cloudfoundry.targets.tab.heading"]
	        [@targetsTab/]
	    [/@dj.contentPane]
	    [@dj.contentPane labelKey="cloudfoundry.credentials.tab.heading"]
	        [@credentialsTab/]
	    [/@dj.contentPane]
	    [@dj.contentPane labelKey="cloudfoundry.proxies.tab.heading"]
	        [@proxiesTab/]
	    [/@dj.contentPane]
	[/@dj.tabContainer]
</body>
</html>

[#macro targetsTab]	
	[#if credentials!?size > 0]	
		<div class="toolbar">
			<div class="aui-toolbar inline">
				<ul class="toolbar-group">
					<li class="toolbar-item">
						<a class="toolbar-trigger" href="[@ww.url action='addTarget' namespace='/admin/cloudfoundry' /]">[@ww.text name='cloudfoundry.global.add.target' /]</a>
					</li>
				</ul>
			</div>
		</div>
	[/#if]
	<p>[@ww.text name='cloudfoundry.global.targets.description' /]</p>
	[@ui.bambooPanel titleKey='cloudfoundry.global.targets.list.heading']
		[#if targets!?size > 0]	
		<table id="targets" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.targets.list.heading.target' /]</th>
				<th class="valueCell">[@ww.text name='cloudfoundry.global.targets.list.heading.configuration' /]</th>
				<th class="operations">[@ww.text name='cloudfoundry.global.targets.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach target in targets]
					<tr>
						<td class="labelPrefixCell">
							<a href="${req.contextPath}/admin/cloudfoundry/viewTarget.action?targetId=${target.ID}">${target.name}</a><br />
							[#if target.description?has_content]
								<span class="subGrey">${target.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							[@ww.text name='cloudfoundry.global.targets.list.url' /]: <a href="${target.url}">${target.url}</a><br />
							[@ww.text name='cloudfoundry.global.targets.list.credentials' /]: ${target.credentials.name}<br />
							[#if target.proxy?has_content]
								[@ww.text name='cloudfoundry.global.targets.list.proxy' /]: ${target.proxy.name}<br />
							[/#if]
							[#if target.disableForBuildPlans]
								[@ww.text name='cloudfoundry.global.targets.list.disableForBuildPlans' /]<br />
							[/#if]							
						</td>
						<td class="operations">
							<a href="${req.contextPath}/admin/cloudfoundry/editTarget.action?targetId=${target.ID}">[@ww.text name='cloudfoundry.global.edit.target' /]</a>
							| <a href="${req.contextPath}/admin/cloudfoundry/deleteTarget.action?targetId=${target.ID}">[@ww.text name='cloudfoundry.global.delete.target' /]</a>
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='cloudfoundry.global.targets.none']
				[#if credentials!?size > 0]	
					[@ww.text name='cloudfoundry.global.targets.none.help' /]
				[#else]
					[@ww.text name='cloudfoundry.global.targets.none.no.credentials.help' /] <a class="toolbar-trigger" href="[@ww.url action='addCredentials' namespace='/admin/cloudfoundry' /]">[@ww.text name='cloudfoundry.global.add.credential' /]</a>
				[/#if]
			[/@ui.messageBox]
		[/#if]
	[/@ui.bambooPanel]
[/#macro]

[#macro credentialsTab]	
	<div class="toolbar">
		<div class="aui-toolbar inline">
			<ul class="toolbar-group">
				<li class="toolbar-item">
					<a class="toolbar-trigger" href="[@ww.url action='addCredentials' namespace='/admin/cloudfoundry' /]">[@ww.text name='cloudfoundry.global.add.credential' /]</a>
				</li>
			</ul>
		</div>
	</div>
	<p>[@ww.text name='cloudfoundry.global.credentials.description' /]</p>
	[@ui.bambooPanel titleKey='cloudfoundry.global.credentials.list.heading']
		[#if credentials!?size > 0]	
		<table id="credentials" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.credentials.list.heading.credential' /]</th>
				<th class="valueCell">[@ww.text name='cloudfoundry.global.credentials.list.heading.configuration' /]</th>
				<th class="operations">[@ww.text name='cloudfoundry.global.credentials.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach credential in credentials]
					<tr>
						<td class="labelPrefixCell">
							${credential.name}<br />
							[#if credential.description?has_content]
								<span class="subGrey">${credential.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							<b>[@ww.text name='cloudfoundry.global.credentials.list.username' /]:</b> ${credential.username}<br />
						</td>
						<td class="operations">
							<a href="${req.contextPath}/admin/cloudfoundry/editCredentials.action?credentialsId=${credential.ID}">[@ww.text name='cloudfoundry.global.edit.credential' /]</a>
							| <a href="${req.contextPath}/admin/cloudfoundry/deleteCredentials.action?credentialsId=${credential.ID}">[@ww.text name='cloudfoundry.global.delete.credential' /]</a>
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='cloudfoundry.global.credentials.none']
				[@ww.text name='cloudfoundry.global.credentials.none.help' /]
			[/@ui.messageBox]
		[/#if]
	[/@ui.bambooPanel]
[/#macro]

[#macro proxiesTab]	
	<div class="toolbar">
		<div class="aui-toolbar inline">
			<ul class="toolbar-group">
				<li class="toolbar-item">
					<a class="toolbar-trigger" href="[@ww.url action='addProxy' namespace='/admin/cloudfoundry' /]">[@ww.text name='cloudfoundry.global.add.proxy' /]</a>
				</li>
			</ul>
		</div>
	</div>
	<p>[@ww.text name='cloudfoundry.global.proxies.description' /]</p>
	[@ui.bambooPanel titleKey='cloudfoundry.global.proxies.list.heading']
		[#if targets!?size > 0]	
		<table id="proxies" class="aui" width="100%">
			<thead><tr>
				<th class="labelPrefixCell">[@ww.text name='cloudfoundry.global.proxies.list.heading.target' /]</th>
				<th class="valueCell">[@ww.text name='cloudfoundry.global.proxies.list.heading.configuration' /]</th>
				<th class="operations">[@ww.text name='cloudfoundry.global.proxies.list.heading.operations' /]</th>
			</tr></thead>
			<tbody>
				[#foreach proxy in proxies]
					<tr>
						<td class="labelPrefixCell">
							${proxy.name}<br />
							[#if proxy.description?has_content]
								<span class="subGrey">${proxy.description}</span>
							[/#if]
						</td>
						<td class="valueCell">
							[@ww.text name='cloudfoundry.global.proxies.list.host' /]: ${proxy.host}<br />
							[@ww.text name='cloudfoundry.global.proxies.list.port' /]: ${proxy.port}<br />
						</td>
						<td class="operations">
							<a href="${req.contextPath}/admin/cloudfoundry/editProxy.action?proxyId=${proxy.ID}">[@ww.text name='cloudfoundry.global.edit.proxy' /]</a>
							| <a href="${req.contextPath}/admin/cloudfoundry/deleteProxy.action?proxyId=${proxy.ID}">[@ww.text name='cloudfoundry.global.delete.proxy' /]</a>
						</td>
					</tr>
				[/#foreach]
			</tbody>
		</table>			
		[#else]
			[@ui.messageBox type='warning' titleKey='cloudfoundry.global.proxies.none']
				[@ww.text name='cloudfoundry.global.proxies.none.help' /]
			[/@ui.messageBox]
		[/#if]
	[/@ui.bambooPanel]
[/#macro]
